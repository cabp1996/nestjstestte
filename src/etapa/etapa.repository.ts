import { Repository, EntityRepository } from 'typeorm';
import { Etapa } from './etapa.entity';
import { InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { EtapaAgregarDto } from './DTO/guardar-etapa.dto';

@EntityRepository(Etapa)
export class EtapaRepository extends Repository<Etapa>{

  async getEtapas(): Promise<Etapa[]> {
    const etapas = await this.find();

    if (!etapas) {
      throw new NotFoundException("No se logró recuperar las etapas.");
    }

    return etapas;
  }

  async guardarEtapa(etapaCrearDto: EtapaAgregarDto): Promise<Etapa> {

    const { id, nombre } = etapaCrearDto;

    const nuevaEtapa = new Etapa();
    nuevaEtapa.nombre = nombre;
    nuevaEtapa.id = id;

    try {
      await nuevaEtapa.save();
    } catch (error) {
      throw new InternalServerErrorException();
    }

    return nuevaEtapa;

  }
}
