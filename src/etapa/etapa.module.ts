import { Module } from '@nestjs/common';
import { EtapaController } from './etapa.controller';
import { EtapaService } from './etapa.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EtapaRepository } from './etapa.repository';

@Module({
  imports:[TypeOrmModule.forFeature([EtapaRepository])],
  controllers: [EtapaController],
  providers: [EtapaService]
})
export class EtapaModule {}
