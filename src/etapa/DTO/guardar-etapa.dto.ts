
export class EtapaAgregarDto {
    nombre: string;
    id: number;

    constructor(id: number, nombre: string) {
        this.nombre = nombre;
        this.id = id;
    }

}
