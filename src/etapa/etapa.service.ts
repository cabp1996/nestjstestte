import { Injectable, NotFoundException } from '@nestjs/common';
import { EtapaRepository } from './etapa.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Etapa } from './etapa.entity';
import { EtapaAgregarDto } from './DTO/guardar-etapa.dto';

@Injectable()
export class EtapaService {

  constructor(@InjectRepository(EtapaRepository) private _repositoryEtapa: EtapaRepository) {
  }


  cargarData() {
    this.revisarEtapas().then(
      (etapas: Etapa[]) => {
        if (etapas) {
          if (etapas.length != 3) {
            const etapa1 = new EtapaAgregarDto(1, 'Creación');
            const etapa2 = new EtapaAgregarDto(2, 'Exploración');
            const etapa3 = new EtapaAgregarDto(3, 'Compartir');
            this._repositoryEtapa.guardarEtapa(etapa1);
            this._repositoryEtapa.guardarEtapa(etapa2);
            this._repositoryEtapa.guardarEtapa(etapa3);
          }
        }
      }
    );
  }

  async revisarEtapas(): Promise<Etapa[]> {
    return this._repositoryEtapa.find();
  }

  async getEtapas(): Promise<Etapa[]> {
    return this._repositoryEtapa.getEtapas();
  }
}
