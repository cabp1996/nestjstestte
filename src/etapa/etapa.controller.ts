import { Controller, Get, UseGuards } from '@nestjs/common';
import { EtapaService } from './etapa.service';
import { Etapa } from './etapa.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('etapa')
export class EtapaController {

  constructor(private readonly _servicioEtapas: EtapaService) {

  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  getEtapas(): Promise<Etapa[]> {
    return this._servicioEtapas.getEtapas();
  }

  @Get('cargar')
  cargarData(): Promise<void> {
    this._servicioEtapas.cargarData();
    return;
  }

}
