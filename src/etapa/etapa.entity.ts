import { Taller } from '../taller/taller.entity';
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { HistoricoTaller } from '../historico-taller/historico-taller.entity';

@Entity()
export class Etapa extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @OneToMany(type => HistoricoTaller, historico => historico.etapa)
  historicos: HistoricoTaller[];

  @OneToMany(type => Taller, taller => taller.etapa)
  talleres: Taller[];

}
