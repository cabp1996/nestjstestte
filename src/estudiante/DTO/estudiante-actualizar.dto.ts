import { Grado } from "../../grado/grado.entity";
import { Persona } from "../../persona/persona.entity";
import { MaxLength, IsString } from "class-validator";

export class EstudianteActualizarDto {

  @IsString()
  @MaxLength(100,{message:'Las observaciones del estudiante no deben sobrepasar de los 100 caracteres alfanuméricos.'})
  observaciones: string;

  grado: Grado;

  representante: Persona;
}
