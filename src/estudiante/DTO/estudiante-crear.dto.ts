import { Persona } from "../../persona/persona.entity";
import { Grado } from "../../grado/grado.entity";
import { IsString, MaxLength } from "class-validator";

export class EstudianteCrearDto {

  @IsString()
  @MaxLength(10,{message:'Las observaciones del estudiante no deben sobrepasar de los 100 caracteres alfanuméricos.'})
  observaciones: string;

  grado: Grado;
  representante: Persona;
  persona: Persona;
}
