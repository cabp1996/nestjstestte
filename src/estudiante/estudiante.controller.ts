import { Controller, Get, Post, Body, Patch, Param, ParseIntPipe, UseGuards, ValidationPipe } from '@nestjs/common';
import { EstudianteService } from './estudiante.service';
import { Estudiante } from './estudiante.entity';
import { EstudianteCrearDto } from './DTO/estudiante-crear.dto';
import { EstudianteActualizarDto } from './DTO/estudiante-actualizar.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('estudiante')
@UseGuards(AuthGuard('jwt'))
export class EstudianteController {

  constructor(private readonly _servicioEstudiante: EstudianteService) {

  }

  @Get()
  obtenerEstudiantes(): Promise<Estudiante[]> {
    return this._servicioEstudiante.getEstudiantes();
  }

  @Get('/:id')
  obtenerEstudianteSegundId(@Param('id') idEstudiante: number): Promise<Estudiante> {
    return this._servicioEstudiante.getEstudianteByID(idEstudiante);
  }


  @Get('representante/:id')
  obtenerEstudiantePorRepresentanteID(@Param('id') idRepresentante: number): Promise<Estudiante> {
    return this._servicioEstudiante.getEstudianteByRepresentanteID(idRepresentante);
  }

  @Post()
  createEstudiante(@Body(ValidationPipe) crearEstudianteDto: EstudianteCrearDto): Promise<Estudiante> {
    return this._servicioEstudiante.createEstudiante(crearEstudianteDto);
  }

  @Patch('/:id')
  actualizarEstudiante(@Param('id', ParseIntPipe) id: number, @Body(ValidationPipe) actualizarEstudianteDto: EstudianteActualizarDto): Promise<Estudiante> {
    return this._servicioEstudiante.actualizarEstudiante(id, actualizarEstudianteDto);
  }
}
