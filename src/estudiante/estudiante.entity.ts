import { BaseEntity, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, ManyToOne } from "typeorm";
import { Persona } from "../persona/persona.entity";
import { HistoricoTaller } from "../historico-taller/historico-taller.entity";
import { RegistroTaller } from "../registro-taller/registro-taller.entity";
import { Grado } from "../grado/grado.entity";
import { Imagen } from '../imagen/imagen.entity';

@Entity()
export class Estudiante extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  observaciones: string;

  @OneToMany(type => Imagen, imagen => imagen.estudiante)
  imagenes: Imagen[];

  @OneToOne(type => Persona,{ cascade: true, onDelete: 'SET NULL'})
  @JoinColumn({ name: 'fkRepresentante' })
  representante: Persona;

  @OneToOne(type => Persona, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkPersona' })
  persona: Persona;

  @OneToMany(type => RegistroTaller, registro => registro.estudiante)
  registros: RegistroTaller[];

  @ManyToOne(type => Grado, grado => grado.estudiantes)
  @JoinColumn({ name: 'fkGrado' })
  grado: Grado;
}
