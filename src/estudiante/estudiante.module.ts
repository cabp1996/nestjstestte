import { Module } from '@nestjs/common';
import { EstudianteService } from './estudiante.service';
import { EstudianteController } from './estudiante.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EstudianteRepository } from './estudiante.repository';

@Module({
  imports: [TypeOrmModule.forFeature([EstudianteRepository])],
  providers: [EstudianteService],
  controllers: [EstudianteController],
  exports: [EstudianteService]
})
export class EstudianteModule { }
