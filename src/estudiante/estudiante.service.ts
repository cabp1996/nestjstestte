import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EstudianteRepository } from './estudiante.repository';
import { EstudianteCrearDto } from './DTO/estudiante-crear.dto';
import { Estudiante } from './estudiante.entity';
import { EstudianteActualizarDto } from './DTO/estudiante-actualizar.dto';

@Injectable()
export class EstudianteService {

  constructor(@InjectRepository(EstudianteRepository) private _repositoryEstudiante: EstudianteRepository) {

  }

  async getEstudiantes(): Promise<Estudiante[]> {
    return this._repositoryEstudiante.getEstudiantes();
  }

  async getEstudianteByID(id: number): Promise<Estudiante> {
    return this._repositoryEstudiante.getEstudianteByID(id);
  }

  async getEstudianteByRepresentanteID(id: number): Promise<Estudiante> {
    return this._repositoryEstudiante.getEstudianteByRepresentanteID(id);
  }

  async createEstudiante(estudianteCrearDto: EstudianteCrearDto): Promise<Estudiante> {
    return this._repositoryEstudiante.createEstudiante(estudianteCrearDto);
  }

  async actualizarEstudiante(idEstudiante: number, estudianteActualizarDto: EstudianteActualizarDto) {
    return this._repositoryEstudiante.actualizarEstudiante(idEstudiante, estudianteActualizarDto);
  }

}
