import { Repository, BaseEntity, EntityRepository } from "typeorm";
import { Estudiante } from "./estudiante.entity";
import { NotFoundException, ConflictException, InternalServerErrorException, Query } from '@nestjs/common';
import { EstudianteCrearDto } from "./DTO/estudiante-crear.dto";
import { EstudianteActualizarDto } from "./DTO/estudiante-actualizar.dto";

@EntityRepository(Estudiante)
export class EstudianteRepository extends Repository<Estudiante>{

  public async getEstudiantes(): Promise<Estudiante[]> {
    const estudiantes = this.find({
      join: {
        alias: 'estudiante',
        leftJoinAndSelect: {
          representante: 'estudiante.representante',
          persona: 'estudiante.persona',
          registros: 'estudiante.registros',
          imagenes: 'estudiante.imagenes',
          grado: 'estudiante.grado'
        },
      },
    });

    if (!estudiantes) {
      throw new NotFoundException('No se hallaron estudiantes');
    }

    return estudiantes;
  }

  async getEstudianteByID(id: number): Promise<Estudiante> {

    const estudiante = this.findOne(id, {
      join: {
        alias: 'estudiante',
        leftJoinAndSelect: {
          representante: 'estudiante.representante',
          persona: 'estudiante.persona',
          registros: 'estudiante.registros',
          imagenes: 'estudiante.imagenes',
          grado: 'estudiante.grado'
        },
      },
    });
    if (!estudiante) {
      throw new NotFoundException('No se hallo el estudiante con ID: ' + id);
    }

    return estudiante;
  }

  async getEstudianteByRepresentanteID(id: number): Promise<Estudiante> {

    const estudiante = this.createQueryBuilder('estudiante')
      .where('estudiante.representante.id=' + id).getOne();

    if (!estudiante) {
      throw new NotFoundException('No se hallo el estudiante con representante ID: ' + id);
    }

    return estudiante;
  }

  async createEstudiante(estudianteCrearDto: EstudianteCrearDto): Promise<Estudiante> {
    const { grado, persona, representante, observaciones } = estudianteCrearDto;

    const nuevoEstudiante = new Estudiante();
    nuevoEstudiante.grado = grado;
    nuevoEstudiante.persona = persona;
    nuevoEstudiante.representante = representante;
    nuevoEstudiante.observaciones = observaciones;

    await nuevoEstudiante.save();

    return nuevoEstudiante;

  }

  async actualizarEstudiante(idEstudiante: number, estudianteActualizado: EstudianteActualizarDto): Promise<Estudiante> {
    const { grado, representante, observaciones } = estudianteActualizado;

    const estudiante = await this.getEstudianteByID(idEstudiante);
    estudiante.grado = grado;
    estudiante.representante = representante;
    estudiante.observaciones = observaciones;

    try {
      await estudiante.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Esa persona ya tiene un representado.');
      } else {
        throw new InternalServerErrorException();
      }
    }


    return estudiante;

  }
}
