import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GradoRepository } from './grado.repository';
import { Grado } from './grado.entity';
import {GradoAgregarDto} from './DTO/guardar-grado.dto';

@Injectable()
export class GradoService {

  constructor(@InjectRepository(GradoRepository) private _repositoryGrado: GradoRepository) {

  }

  async obtenerGrados(): Promise<Grado[]> {
    return this._repositoryGrado.obtenerGrados();
  }
  
  async guardarGrado(gradoCrearDto: GradoAgregarDto): Promise<Grado> {
    return this._repositoryGrado.guardarGrado(gradoCrearDto);
  }



}
