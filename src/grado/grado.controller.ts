import { Controller, Get,Post, UseGuards,Body } from '@nestjs/common';
import { Grado } from './grado.entity';
import { GradoService } from './grado.service';
import { AuthGuard } from '@nestjs/passport';
import {GradoAgregarDto} from './DTO/guardar-grado.dto';

@Controller('grado')
@UseGuards(AuthGuard('jwt'))
export class GradoController {

  constructor(private readonly _servicioGrado: GradoService) {

  }

  @Get()
  obtenerGrados(): Promise<Grado[]> {
    return this._servicioGrado.obtenerGrados();
  }
  
  @Post()
  guardarGrado(@Body() gradoCrearDto: GradoAgregarDto): Promise<Grado> {
    return this._servicioGrado.guardarGrado(gradoCrearDto);
  }
}
