import { Module } from '@nestjs/common';
import { GradoController } from './grado.controller';
import { GradoService } from './grado.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GradoRepository } from './grado.repository';

@Module({
  imports: [TypeOrmModule.forFeature([GradoRepository])],
  controllers: [GradoController],
  providers: [GradoService]
})
export class GradoModule { }
