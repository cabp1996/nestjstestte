import { Repository, BaseEntity, EntityRepository } from "typeorm";
import { Grado } from "./grado.entity";
import { NotFoundException,InternalServerErrorException,ConflictException } from "@nestjs/common";
import {GradoAgregarDto} from './DTO/guardar-grado.dto';

@EntityRepository(Grado)
export class GradoRepository extends Repository<Grado>{

  async obtenerGrados(): Promise<Grado[]> {
    const grados = await this.find({
      relations: ['estudiantes', 'estudiantes.persona', 'estudiantes.representante', 'estudiantes.grado']
    });

    if (!grados) {
      throw new NotFoundException("No se ha podido recuperar los grados.");
    }
    return grados;
  }
  
  async guardarGrado(gradoCrearDto: GradoAgregarDto):Promise<Grado>{
	
	const {grado}=gradoCrearDto;
	
	const nuevoGrado=new Grado();
	nuevoGrado.grado=grado;
	
	try{
		await nuevoGrado.save();
	}catch(error){
	  if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe ese grado registrado.');
      } else {
        throw new InternalServerErrorException();
      }
	}
	
	return nuevoGrado;
	
  }

}
