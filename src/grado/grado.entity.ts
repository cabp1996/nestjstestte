import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToMany,Unique } from "typeorm";
import { Estudiante } from "../estudiante/estudiante.entity";

@Entity()
@Unique(['grado'])
export class Grado extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  grado: string;

  @OneToMany(type => Estudiante, estudiante => estudiante.grado)
  estudiantes: Estudiante[];
}
