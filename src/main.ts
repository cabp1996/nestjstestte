import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
//import * as config from 'config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.setGlobalPrefix('api');
  // const serverConfig = config.get('server')
  let port = process.env.PORT || 3000;
  await app.listen(Number(port));
}
bootstrap();
