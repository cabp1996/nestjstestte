import { Module } from '@nestjs/common';
import { RegistroTallerService } from './registro-taller.service';
import { RegistroTallerController } from './registro-taller.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegistroTallerRepository } from './registro-taller.repository';
import { EstudianteService } from '../estudiante/estudiante.service';
import { EstudianteRepository } from '../estudiante/estudiante.repository';
import { TallerRepository } from '../taller/taller.repository';
import { TallerService } from '../taller/taller.service';
import { TallerModule } from '../taller/taller.module';

@Module({
  imports: [TypeOrmModule.forFeature([RegistroTallerRepository, EstudianteRepository, TallerRepository]), TallerModule],
  providers: [RegistroTallerService, EstudianteService, TallerService],
  controllers: [RegistroTallerController]
})
export class RegistroTallerModule { }
