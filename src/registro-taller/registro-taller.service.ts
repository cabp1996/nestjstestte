import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RegistroTallerRepository } from './registro-taller.repository';
import { RegistroTallerCrearDto } from './DTO/registro-taller-crear.dto';
import { RegistroTaller } from './registro-taller.entity';
import { TallerService } from '../taller/taller.service';

@Injectable()
export class RegistroTallerService {

  constructor(@InjectRepository(RegistroTallerRepository)
  private _repositoryRegistroTaller: RegistroTallerRepository,
    private readonly _tallerService: TallerService) {

  }

  async crearRegistroTaller(crearRegistroTaller: RegistroTallerCrearDto): Promise<RegistroTaller> {
    const registro = await this._repositoryRegistroTaller.crearRegistroTaller(crearRegistroTaller);

    if (registro) {
      const tallerActualizado = await this._tallerService.updateTallerParticipantes(registro.taller.id);
	    return registro;
    }
  }

  async obtenerRegistros(idTaller?: number): Promise<RegistroTaller[]> {

    return this._repositoryRegistroTaller.obtenerRegistros(idTaller);
  }

  async obtenerRegistrosConHistoricos(id: number, idEstudiante: number): Promise<RegistroTaller[]> {
    return this._repositoryRegistroTaller.obtenerRegistrosConHistoricos(id, idEstudiante);
  }

  async obtenerRegistrosSegunTallerId(idTaller: number): Promise<RegistroTaller[]> {
    return this._repositoryRegistroTaller.obtenerRegistrosSegunIdTaller(idTaller);
  }

  async eliminarRegistro(idRegistro: number): Promise<void> {

    const registro = await this._repositoryRegistroTaller.findOne(idRegistro, {
      join: {
        alias: 'registro',
        leftJoinAndSelect: {
          taller: 'registro.taller',
        }
      }
    });

    const resultado = await this._repositoryRegistroTaller.delete(idRegistro);

    if (resultado.affected === 0) {
      throw new NotFoundException('Error, no existe ese registro con id ' + idRegistro);

    } else {

      this._tallerService.updateTallerParticipantes(registro.taller.id);

    }

  }

}
