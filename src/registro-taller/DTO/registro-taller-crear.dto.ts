import { Estudiante } from "../../estudiante/estudiante.entity";
import { Taller } from "../../taller/taller.entity";

export class RegistroTallerCrearDto {
  estudiante: Estudiante;
  taller: Taller;
}
