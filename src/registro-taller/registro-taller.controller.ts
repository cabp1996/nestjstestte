import { Controller, Get, Post, Body, Delete, Param, ParseIntPipe, Query, UseGuards } from '@nestjs/common';
import { RegistroTallerService } from './registro-taller.service';
import { RegistroTaller } from './registro-taller.entity';
import { RegistroTallerCrearDto } from './DTO/registro-taller-crear.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('registro-taller')
@UseGuards(AuthGuard('jwt'))
export class RegistroTallerController {

  constructor(private readonly _servicioRegistroTaller: RegistroTallerService) {

  }


  @Get()
  getRegistrosTalleresEstudiante(): Promise<RegistroTaller[]> {

      return this._servicioRegistroTaller.obtenerRegistros();

  }

  @Get('historicos')
  getRegistrosTalleres(
    @Query('tallerHistorico') tallerHistorico?: number,
    @Query('idEstudiante') idEstudiante?: number): Promise<RegistroTaller[]> {

    if (tallerHistorico) {

      return this._servicioRegistroTaller.obtenerRegistrosConHistoricos(tallerHistorico, undefined);
    } else if (idEstudiante) {

      return this._servicioRegistroTaller.obtenerRegistrosConHistoricos(undefined, idEstudiante);
    } else {
      return this._servicioRegistroTaller.obtenerRegistros();
    }

  }


  @Get('/:id')
  getRegistrosTalleresSegunTallerId(@Param('id', ParseIntPipe) idTaller: number): Promise<RegistroTaller[]> {
    return this._servicioRegistroTaller.obtenerRegistrosSegunTallerId(idTaller);
  }



  @Post()
  createRegistroTaller(@Body() crearRegistroDto: RegistroTallerCrearDto): Promise<RegistroTaller> {
    return this._servicioRegistroTaller.crearRegistroTaller(crearRegistroDto);
  }

  @Delete('/:id')
  deleteRegistroTaller(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this._servicioRegistroTaller.eliminarRegistro(id);
  }


}
