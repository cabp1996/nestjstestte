import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, Unique } from "typeorm";
import { Taller } from "../taller/taller.entity";
import { Estudiante } from "../estudiante/estudiante.entity";
import { HistoricoTaller } from "../historico-taller/historico-taller.entity";

@Entity()
export class RegistroTaller extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Taller, taller => taller.registros, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkTaller' })
  taller: Taller;

  @ManyToOne(type => Estudiante, estudiante => estudiante.registros, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkEstudiante' })
  estudiante: Estudiante;

  @OneToMany(type => HistoricoTaller, historico => historico.registroTaller)
  historicos: HistoricoTaller[];

}
