import { Repository, EntityRepository } from "typeorm";
import { RegistroTaller } from "./registro-taller.entity";
import { RegistroTallerCrearDto } from "./DTO/registro-taller-crear.dto";
import { NotFoundException, ConflictException, InternalServerErrorException } from "@nestjs/common";
import { Estudiante } from "../estudiante/estudiante.entity";
import { Taller } from '../taller/taller.entity';

@EntityRepository(RegistroTaller)
export class RegistroTallerRepository extends Repository<RegistroTaller>{


  async crearRegistroTaller(crearRegistroTaller: RegistroTallerCrearDto): Promise<RegistroTaller> {

    const { estudiante, taller } = crearRegistroTaller;
    const nuevoRegistro = new RegistroTaller();
    nuevoRegistro.estudiante = estudiante;
    nuevoRegistro.taller = taller;

    const response = await this.findAndCount({ taller });

    if (response[1] < taller.numeroParticipantes) {

      try {
        await nuevoRegistro.save();
      } catch (error) {
        if (error.code === 'ER_DUP_ENTRY') {
          throw new ConflictException('El estudiante ya se encuentra registrado en este taller.');
        } else {
          throw new InternalServerErrorException();
        }
      }

    } else {
      throw new InternalServerErrorException('Este taller no tiene más vacantes.');
    }

    return nuevoRegistro;
  }


  async obtenerRegistrosConHistoricos(id?: number, idEstudiante?: number): Promise<RegistroTaller[]> {

    const registros = await this.find({
      relations: ['estudiante',
        'estudiante.persona',
        'estudiante.registros',
        'estudiante.grado',
        'taller',
        'taller.registros',
        'historicos',
        'historicos.etapa']
    });
	


    if (!registros) {
      throw new NotFoundException('No se hallaron registros.');
    }

    if (id) {
      return registros.filter(registro => registro.id === Number(id));
    } else if (idEstudiante) {
      let results:RegistroTaller[]= registros.filter((registro: RegistroTaller) => registro.estudiante.id === Number(idEstudiante) && registro.taller.estado === 'Terminado');
    	
		return results;
	}
  }

  async obtenerRegistros(idTaller?: number): Promise<RegistroTaller[]> {

    let registros: RegistroTaller[] = [];

    if (idTaller) {

      const tallerAux = new Taller();
      tallerAux.id = idTaller;

      registros = await this.find({
        relations: ['estudiante', 'estudiante.persona', 'estudiante.registros', 'estudiante.grado', 'taller', 'taller.registros'],
        where: { taller: tallerAux }
      });

    } else {
      registros = await this.find({
        relations: ['estudiante', 'estudiante.persona', 'estudiante.registros', 'estudiante.grado', 'taller', 'taller.registros']
      });
    }


    if (!registros) {
      throw new NotFoundException('No se hallaron registros.');
    }

    return registros;
  }

  async obtenerRegistrosSegunIdTaller(idTaller: number): Promise<RegistroTaller[]> {

    /*const registros = await this.find({
      join: {
        alias: "registroTaller",
        leftJoinAndSelect: {
          estudiante: "registroTaller.estudiante",
          taller: "registroTaller.taller",
        },
      }
    });*/

    const registros = await this.find({
      relations: ['estudiante', 'estudiante.persona', 'estudiante.registros', 'estudiante.grado', 'estudiante.imagenes', 'taller', 'taller.registros']

    });

    if (!registros) {
      throw new NotFoundException('No se hallaron registros.');
    }

    return registros.filter((registro) => registro.taller.id === idTaller);
  }


}
