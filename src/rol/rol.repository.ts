import { Repository, EntityRepository } from "typeorm";
import { Rol } from './rol.entity';
import { InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { RolCrearDto } from './DTO/rol-crear.dto';

@EntityRepository(Rol)
export class RolRespository extends Repository<Rol>{

  async getRolSegunID(rolId: number): Promise<Rol> {

    const rol = await this.findOne(rolId);

    if (!rol) {
      throw new NotFoundException("No se ha encontrado ese rol.");
    }
    return rol;
  }

  async getRoles(): Promise<Rol[]> {
    const roles = this.find();
    if (!roles) {
      throw new NotFoundException("No se ha podido recuperar roles.");
    }
    return roles;
  }


  async guardarRol(rolCrearDto: RolCrearDto): Promise<Rol> {

    const { id, nombre } = rolCrearDto;

    const nuevoRol = new Rol();
    nuevoRol.nombre = nombre;
    nuevoRol.id = id;

    try {
      await nuevoRol.save();
    } catch (error) {
      throw new InternalServerErrorException();
    }

    return nuevoRol;

  }

}
