import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RolRespository } from './rol.repository';
import { Rol } from './rol.entity';
import { RolCrearDto } from './DTO/rol-crear.dto';

@Injectable()
export class RolService {

  constructor(@InjectRepository(RolRespository)
  private _repositoryRol: RolRespository) {
  }

  cargarData() {
    this.revisarRoles().then(
      (roles: Rol[]) => {
        if (roles) {
          if (roles.length != 4) {
            const rolAdministrador = new RolCrearDto(1, 'Administrador');
            const rolTutor = new RolCrearDto(2, 'Tutor');
            const rolRepresentante = new RolCrearDto(3, 'Representante');
            const rolAutoridad = new RolCrearDto(4, 'Autoridad');

            this._repositoryRol.guardarRol(rolAdministrador);
            this._repositoryRol.guardarRol(rolTutor);
            this._repositoryRol.guardarRol(rolRepresentante);
            this._repositoryRol.guardarRol(rolAutoridad);
          }
        }
      }
    );
  }

  async getRolSegunID(rolId: number): Promise<Rol> {
    return this._repositoryRol.getRolSegunID(rolId);
  }

  async getRoles(): Promise<Rol[]> {
    return this._repositoryRol.getRoles();
  }

  async revisarRoles(): Promise<Rol[]> {
    return this._repositoryRol.find();
  }


}
