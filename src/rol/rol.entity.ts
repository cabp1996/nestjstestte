import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Usuario } from '../usuario/usuario.entity';
import { type } from "os";


@Entity()
export class Rol extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @OneToMany(type => Usuario, user => user.rol)
  usuarios: Usuario[]
}
