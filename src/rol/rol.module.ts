import { Module } from '@nestjs/common';
import { RolController } from './rol.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolRespository } from './rol.repository';
import { RolService } from './rol.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([RolRespository])
  ],
  controllers: [RolController],
  providers: [RolService]
})
export class RolModule {}
