import { Controller, Get, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { RolService } from './rol.service';
import { Rol } from './rol.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('rol')
export class RolController {

    constructor(private readonly _servicioRol: RolService) {
    }


    @Get()
    @UseGuards(AuthGuard('jwt'))
    getRoles(): Promise<Rol[]> {
        return this._servicioRol.getRoles();
    }

    @Get('cargar')
    cargarData(): Promise<void> {
        this._servicioRol.cargarData();
        return;
    }

    @Get('/:id')
    @UseGuards(AuthGuard('jwt'))
    getRolSegunID(@Param('id', ParseIntPipe) id: number): Promise<Rol> {
        return this._servicioRol.getRolSegunID(id);
    }


}
