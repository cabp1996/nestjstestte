import { Rol } from '../../rol/rol.entity';
import { Persona } from '../../persona/persona.entity';

import { IsString, MinLength, MaxLength } from 'class-validator';

export class UsuarioActualizarDto{

  @IsString()
  @MinLength(8, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  @MaxLength(20, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  user:string;

  rol: Rol;
  persona:Persona;
}
