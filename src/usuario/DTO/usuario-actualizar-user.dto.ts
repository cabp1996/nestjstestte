
import { IsString, MinLength, MaxLength } from 'class-validator';

export class UsuarioActualizarUserDto {

  @IsString()
  @MinLength(8, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  @MaxLength(20, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  user: string;

  @IsString()
  password: string;

}
