import { Rol } from "../../rol/rol.entity";
import { Persona } from '../../persona/persona.entity';
import { IsString, MinLength, MaxLength } from 'class-validator';

export class UsuarioCrearDto {

  @IsString()
  @MinLength(8, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  @MaxLength(20, { message: 'El nombre de usuario debe tener entre 8 y 20 caracteres' })
  user: string;

  @IsString()
  @MinLength(10, { message: 'La contraseña debe tener entre 10 y 15 caracteres' })
  @MaxLength(15, { message: 'La contraseña debe tener entre 10 y 15 caracteres' })
  password: string;

  rol: Rol;
  persona: Persona;

  constructor(user: string, password: string, rol: Rol, persona: Persona) {
    this.user = user;
    this.password = password;
    this.rol = rol;
    this.persona = persona;
  }

}
