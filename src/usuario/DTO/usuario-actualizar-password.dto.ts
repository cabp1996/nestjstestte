import { IsString, MinLength, MaxLength } from 'class-validator';

export class UsuarioActualizarPasswordDto {

  @IsString()
  @MinLength(10, { message: 'La contraseña debe tener entre 10 y 15 caracteres' })
  @MaxLength(15, { message: 'La contraseña debe tener entre 10 y 15 caracteres' })
  passwordNueva: string;

  @IsString()
  password: string;

}
