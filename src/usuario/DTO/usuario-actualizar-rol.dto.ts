
import { IsString } from 'class-validator';
import { Rol } from '../../rol/rol.entity';

export class UsuarioActualizarRolDto {

  rol: Rol;

  @IsString()
  password: string;

}
