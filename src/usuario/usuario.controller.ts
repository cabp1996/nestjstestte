import { Controller, Get, Param, ParseIntPipe, Post, Body, Delete, ValidationPipe, UseGuards, Patch, Query } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { Usuario } from './usuario.entity';
import { UsuarioCrearDto } from './DTO/usuario-create.dto';
import { UsuarioLoginDto } from './DTO/usuario-login.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './usuario.obtener.decorator';
import { UsuarioActualizarDto } from './DTO/usuario-actualizar.dto';
import { UsuarioActualizarUserDto } from './DTO/usuario-actualizar-user.dto';
import { UsuarioActualizarRolDto } from './DTO/usuario-actualizar-rol.dto';
import { UsuarioActualizarPasswordDto } from './DTO/usuario-actualizar-password.dto';


@Controller('usuario')
export class UsuarioController {

  constructor(private readonly _servicioUsuario: UsuarioService) {
  }


  @Get()
  @UseGuards(AuthGuard('jwt'))
  getUsuarios(): Promise<Usuario[]> {
    return this._servicioUsuario.getUsuariosCompletos();
  }

  @Get('check')
  getCheckUsers(): Promise<number> {
    return this._servicioUsuario.revisarUsuarios();
  }

  @Get('loadadmin')
  loadAdmin(): Promise<Usuario> {
    return this._servicioUsuario.cargarAdmin();
  }


  @Get('representantes')
  @UseGuards(AuthGuard('jwt'))
  getUsuariosRepresentantes(@Query() queryParams): Promise<Usuario[]> {

    return this._servicioUsuario.getUsuariosRepresentantesCompletosPorRol(queryParams.idEstudiante);
  }

  @Get('byrol')
  @UseGuards(AuthGuard('jwt'))
  getUsuariosSegunRol(): Promise<Usuario[]> {
    return this._servicioUsuario.getUsuarioPorRol();
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  postUsuario(@Body(ValidationPipe) usuarioCrearDto: UsuarioCrearDto): Promise<Usuario> {
    return this._servicioUsuario.singUp(usuarioCrearDto);
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  getUsuarioSegunID(@Param('id') id: number): Promise<Usuario> {
    return this._servicioUsuario.getUsuarioPorId(id);
  }



  @Post('login')
  loginUsuario(@Body() usuarioLoginDto: UsuarioLoginDto): Promise<{ accessToken: string }> {

    return this._servicioUsuario.autenticarUsuario(usuarioLoginDto);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'))
  deleteUsuario(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this._servicioUsuario.deleteUsuario(id);
  }

  @Post('test')
  @UseGuards(AuthGuard())
  test(@GetUser() user: Usuario) {
    console.log(user);
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'))
  actualizarUsuario(@Param('id', ParseIntPipe) idUsuario: number, @Body(ValidationPipe) usuarioActualizarDto: UsuarioActualizarDto): Promise<Usuario> {
    return this._servicioUsuario.updateUsuario(idUsuario, usuarioActualizarDto);
  }

  @Patch('actualizar/:id/username')
  @UseGuards(AuthGuard('jwt'))
  actualizarUsuarioUsername(@Param('id') idUsuario: number, @Body(ValidationPipe) usuarioActualizarUsernameDto: UsuarioActualizarUserDto): Promise<{ accessToken: string }> {
    return this._servicioUsuario.updateUsuarioUsername(idUsuario, usuarioActualizarUsernameDto);

  }

  @Patch('actualizar/:id/rol')
  @UseGuards(AuthGuard('jwt'))
  actualizarUsuarioRol(@Param('id') idUsuario: number, @Body(ValidationPipe) usuarioActualizarRolDto: UsuarioActualizarRolDto): Promise<Usuario> {
    return this._servicioUsuario.updateUsuarioRol(idUsuario, usuarioActualizarRolDto);
  }

  @Patch('actualizar/:id/password')
  @UseGuards(AuthGuard('jwt'))
  actualizarUsuarioPassword(@Param('id') idUsuario: number, @Body(ValidationPipe) usuarioActualizarPasswordDto: UsuarioActualizarPasswordDto): Promise<{ accessToken: string }> {

    return this._servicioUsuario.updateUsuarioPassword(idUsuario, usuarioActualizarPasswordDto);
  }


}
