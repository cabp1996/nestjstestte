import { EstudianteService } from '../estudiante/estudiante.service';
import { Module } from '@nestjs/common';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioRepository } from './usuario.repository';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt-strategy';
import { EstudianteRepository } from '../estudiante/estudiante.repository';
//import * as config from 'config';

//const jwtConfig = config.get('jwt');

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || 'Gaby',// jwtConfig.secret,
      signOptions: {
        expiresIn: 7200
      }
    }),
    TypeOrmModule.forFeature([UsuarioRepository, EstudianteRepository])
  ],
  controllers: [UsuarioController],
  providers: [UsuarioService, EstudianteService, JwtStrategy],
  exports: [JwtStrategy, PassportModule]
})
export class UsuarioModule { }
