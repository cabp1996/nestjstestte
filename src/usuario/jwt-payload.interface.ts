import { Persona } from "../persona/persona.entity";
import { Rol } from "../rol/rol.entity";
import { Usuario } from "./usuario.entity";

export interface JwtPayload {
  usuario: Usuario;
}
