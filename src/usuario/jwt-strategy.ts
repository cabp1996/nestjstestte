import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { JwtPayload } from './jwt-payload.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioRepository } from './usuario.repository';
import { UnauthorizedException } from '@nestjs/common';
import { Usuario } from './usuario.entity';

//import * as config from 'config';


export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@InjectRepository(UsuarioRepository) private _repositoryUsuario: UsuarioRepository) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'Gaby'//config.get('jwt.secret'), //'Gaby',
    });
  }

  async validate(payload: JwtPayload): Promise<Usuario> {
    const { usuario } = payload;
    const usuarioAValidar = await this._repositoryUsuario.findOne({ user: usuario.user });

    if (!usuarioAValidar) {
      throw new UnauthorizedException();
    }

    return usuarioAValidar;

  }
}
