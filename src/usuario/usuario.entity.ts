import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne, OneToMany, Unique } from "typeorm";
import { Rol } from '../rol/rol.entity';
import { Persona } from '../persona/persona.entity';
import { Taller } from "../taller/taller.entity";
import * as bcrypt from 'bcrypt';

@Entity()
@Unique(['user'])
export class Usuario extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user: string;

  @Column()
  password: string;

  @Column()
  sal: string;

  @ManyToOne(type => Rol, rol => rol.usuarios)
  @JoinColumn({ name: 'fkRol' })
  rol: Rol;

  @OneToOne(type => Persona, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkPersona' })
  persona: Persona;

  @OneToMany(type => Taller, taller => taller.encargado)
  talleres: Taller[];

  async validarPassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.sal);
    return hash === this.password;
  }
}
