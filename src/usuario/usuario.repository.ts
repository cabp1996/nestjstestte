import { UsuarioActualizarRolDto } from './DTO/usuario-actualizar-rol.dto';
import { Repository, EntityRepository, In } from 'typeorm';
import { Usuario } from './usuario.entity';
import { NotFoundException, ConflictException, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { Rol } from '../rol/rol.entity';
import { UsuarioCrearDto } from './DTO/usuario-create.dto';
import { UsuarioLoginDto } from './DTO/usuario-login.dto';
import { UsuarioActualizarDto } from './DTO/usuario-actualizar.dto';

import * as bcrypt from 'bcrypt';
import { JwtPayload } from './jwt-payload.interface';
import { UsuarioActualizarUserDto } from './DTO/usuario-actualizar-user.dto';
import { UsuarioActualizarPasswordDto } from './DTO/usuario-actualizar-password.dto';

@EntityRepository(Usuario)
export class UsuarioRepository extends Repository<Usuario>{


  async getUsuarios(): Promise<Usuario[]> {
    const usuarios = await this.find();
    if (!usuarios) {
      throw new NotFoundException('No se hallaron usuarios.');
    }

    return usuarios;
  }



  async getUsuariosCompletos(): Promise<Usuario[]> {
    const usuarios = await this.find({
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          Persona: 'user.persona'
        }
      }
    });

    if (!usuarios) {
      throw new NotFoundException('No se hallaron usuarios.');
    }

    usuarios.forEach((user) => {
      delete user.password;
      delete user.sal;
    });
    return usuarios;
  }

  async getUsuariosRepresentantesCompletosPorRol(idEstudiante?: number): Promise<Usuario[]> {

    const rolRepresentante = new Rol();
    rolRepresentante.id = 3;
    rolRepresentante.nombre = 'Representante';

    let usuarios: Usuario[];
    let usuariosAux: Usuario[];

    if (!idEstudiante) {
      usuariosAux = await
        this.query('SELECT usuario.id FROM usuario where usuario.fkRol=3 AND usuario.fkPersona NOT IN(SELECT estudiante.fkRepresentante from estudiante WHERE estudiante.fkRepresentante IS NOT NULL);');


    } else {
      usuariosAux = await
        this.query('SELECT usuario.id FROM usuario where usuario.fkRol=3 AND usuario.fkPersona NOT IN(SELECT estudiante.fkRepresentante from estudiante where estudiante.fkRepresentante IS NOT NULL AND estudiante.id!=' + Number(idEstudiante) + ');');
    }

    let usuariosSinRepresentado: number[] = [];
    usuariosAux.forEach((user) => {
      usuariosSinRepresentado.push(Number(Object.values(user)[0]))
    });
    if (usuariosSinRepresentado.length === 0) usuariosSinRepresentado.push(0);
    usuarios = await this.find({
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          Persona: 'user.persona',
        },
      },
      where: {
        rol: rolRepresentante,
        id: In(usuariosSinRepresentado)
      }
    });

    if (!usuarios) {
      throw new NotFoundException('No se hallaron usuarios.');
    }

    usuarios.forEach((user) => {
      delete user.password;
      delete user.sal;
    });

    return usuarios;
  }

  async getUsuarioByCredenciales(user: string, pass: string) {

    const usuario = await this.find({ user: user, password: pass });

    if (!usuario) {
      throw new NotFoundException('Usuario no hallado.');
    }

    return usuario;
  }

  async getUsuarioPorId(idUsuario: number): Promise<Usuario> {

    const usuario = await this.findOne(idUsuario, {
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          Persona: 'user.persona'
        }
      }
    });

    if (!usuario) {
      throw new NotFoundException('No se encontro al usuario.');
    }

    delete usuario.password;
    delete usuario.sal;

    return usuario;
  }

  async getUsuarioPorRol(): Promise<Usuario[]> {
    const usuario = await this.find({
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol'
        }
      }
    });

    if (!usuario) {
      throw new NotFoundException('Usuario no hallado.');
    }

    return usuario;
  }

  async singUp(createUsuarioDto: UsuarioCrearDto): Promise<Usuario> {
    const { user, password, rol, persona } = createUsuarioDto;

    const nuevoUsuario = new Usuario();
    nuevoUsuario.user = user;
    nuevoUsuario.sal = await bcrypt.genSalt();
    nuevoUsuario.rol = rol;
    nuevoUsuario.persona = persona;
    nuevoUsuario.password = await this.hashPassword(password, nuevoUsuario.sal);

    try {
      await nuevoUsuario.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe ese nombre de usuario.');
      } else {
        throw new InternalServerErrorException();
      }
    }

    delete nuevoUsuario.sal;
    delete nuevoUsuario.password;

    return nuevoUsuario;
  }

  async autenticarUsuario(loginUsuarioDto: UsuarioLoginDto): Promise<JwtPayload> {
    const { user, password } = loginUsuarioDto;
    const usuario = await this.findOne({ user }, {
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          persona: 'user.persona'
        }
      }
    });


    if (usuario && await usuario.validarPassword(password)) {

      delete usuario.password;
      delete usuario.sal;

      const usuarioPaylodad: JwtPayload = {
        usuario: usuario
      }
      return usuarioPaylodad;

    } else {
      return null;
    }
  }

  private async hashPassword(password: string, sal: string): Promise<string> {
    return bcrypt.hash(password, sal);
  }

  async updateUsuario(idUsuario: number, actualizarUsuarioDto: UsuarioActualizarDto): Promise<Usuario> {

    const { user, rol, persona } = actualizarUsuarioDto;
    const usuario = await this.getUsuarioPorId(idUsuario);
    usuario.user = user;
    usuario.rol = rol;
	if(persona)usuario.persona=persona;

    delete usuario.password;
    delete usuario.sal;

    try {
      await usuario.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe ese nombre de usuario.');
      } else {
        throw new InternalServerErrorException();
      }
    }


    return usuario;
  }


  async updateUsuarioUsername(idUsuario: number, actualizarUsuarioUsernameDto: UsuarioActualizarUserDto): Promise<JwtPayload> {

    const { user, password } = actualizarUsuarioUsernameDto;
    const usuario = await this.findOne(idUsuario, {
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          persona: 'user.persona'
        }
      }
    });

    if (usuario && await usuario.validarPassword(password)) {

      usuario.user = user;

      try {
        await usuario.save();
      } catch (error) {
        if (error.code === 'ER_DUP_ENTRY') {
          throw new ConflictException('Ya existe ese usuario.');
        } else {
          throw new InternalServerErrorException();
        }
      }


    } else {
      throw new UnauthorizedException('La contraseña no es la correcta.');
    }

    delete usuario.password;
    delete usuario.sal;


    const usuarioPaylodad: JwtPayload = {
      usuario: usuario
    }

    return usuarioPaylodad;

  }

  async updateUsuarioRol(idUsuario: number, actualizarUsuarioRolDto: UsuarioActualizarRolDto): Promise<Usuario> {

    const { rol, password } = actualizarUsuarioRolDto;
    const usuario = await this.findOne(idUsuario);


    if (usuario && await usuario.validarPassword(password)) {

      usuario.rol = rol;

      await usuario.save();

    } else {
      throw new UnauthorizedException('La contraseña no es la correcta.');
    }

    delete usuario.password;
    delete usuario.sal;

    return usuario;

  }

  async updateUsuarioPassword(idUsuario: number, actualizarUsuarioPasswordDto: UsuarioActualizarPasswordDto): Promise<JwtPayload> {

    const { passwordNueva, password } = actualizarUsuarioPasswordDto;
    const usuario = await this.findOne(idUsuario, {
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          rol: 'user.rol',
          persona: 'user.persona'
        }
      }
    });
    if (usuario && await usuario.validarPassword(password)) {

      usuario.sal = await bcrypt.genSalt();
      usuario.password = await this.hashPassword(passwordNueva, usuario.sal);

      await usuario.save();

    } else {
      throw new UnauthorizedException('La contraseña no es la correcta.');
    }

    delete usuario.password;
    delete usuario.sal;

    const usuarioPaylodad: JwtPayload = {
      usuario: usuario
    }

    return usuarioPaylodad;

  }



}

