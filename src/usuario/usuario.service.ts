import { UsuarioActualizarRolDto } from './DTO/usuario-actualizar-rol.dto';
import { UsuarioActualizarUserDto } from './DTO/usuario-actualizar-user.dto';
import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioRepository } from './usuario.repository';
import { Usuario } from './usuario.entity';
import { UsuarioCrearDto } from './DTO/usuario-create.dto';
import { UsuarioLoginDto } from './DTO/usuario-login.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { UsuarioActualizarDto } from './DTO/usuario-actualizar.dto';
import { UsuarioActualizarPasswordDto } from './DTO/usuario-actualizar-password.dto';
import { Rol } from '../rol/rol.entity';
import { Persona } from '../persona/persona.entity';

@Injectable()
export class UsuarioService {

  constructor(@InjectRepository(UsuarioRepository)
  private _repositoryUsuario: UsuarioRepository,
    private _jwtService: JwtService) {

  }

  async cargarAdmin(): Promise<Usuario> {
    const rol: Rol = new Rol();
    rol.id = 1;

    const createUsuarioDto = new UsuarioCrearDto('adminEdu20', '1234567890', rol, null);
    return this._repositoryUsuario.singUp(createUsuarioDto);
  }

  async revisarUsuarios(): Promise<number> {
    return this._repositoryUsuario.count();
  }

  async getUsuarios(): Promise<Usuario[]> {
    return this._repositoryUsuario.getUsuarios();
  }

  async getUsuariosCompletos(): Promise<Usuario[]> {
    return this._repositoryUsuario.getUsuariosCompletos();
  }

  async getUsuariosRepresentantesCompletosPorRol(idEstudiante?: number): Promise<Usuario[]> {
    return this._repositoryUsuario.getUsuariosRepresentantesCompletosPorRol(idEstudiante);
  }

  async getUsuarioByCredenciales(user: string, pass: string) {
    return this._repositoryUsuario.getUsuarioByCredenciales(user, pass);
  }

  async getUsuarioPorId(id: number): Promise<Usuario> {
    return this._repositoryUsuario.getUsuarioPorId(id);
  }

  async getUsuarioPorRol(): Promise<Usuario[]> {
    return this._repositoryUsuario.getUsuarioPorRol();
  }

  async singUp(createUsuarioDto: UsuarioCrearDto): Promise<Usuario> {
    return this._repositoryUsuario.singUp(createUsuarioDto);
  }

  async autenticarUsuario(loginUsuarioDto: UsuarioLoginDto): Promise<{ accessToken: string }> {
    const user = await this._repositoryUsuario.autenticarUsuario(loginUsuarioDto);

    if (!user) {
      throw new UnauthorizedException('Credenciales inválidas.');
    }

    const payload: JwtPayload = user;
    const accessToken = await this._jwtService.sign(payload);

    return { accessToken };
  }

  async deleteUsuario(idUsuario: number): Promise<void> {
    const resultado = await this._repositoryUsuario.delete(idUsuario);

    if (resultado.affected === 0) {
      throw new NotFoundException("No se encontró un usuario con el id: " + idUsuario);
    }
  }

  async updateUsuario(idUsuario: number, usuarioActualizarDto: UsuarioActualizarDto): Promise<Usuario> {
    return this._repositoryUsuario.updateUsuario(idUsuario, usuarioActualizarDto);
  }

  async updateUsuarioUsername(idUsuario: number, usuarioActualizarUsernameDto: UsuarioActualizarUserDto): Promise<{ accessToken: string }> {
    const user = await this._repositoryUsuario.updateUsuarioUsername(idUsuario, usuarioActualizarUsernameDto);

    if (!user) {
      throw new UnauthorizedException('Credenciales inválidas.');
    }

    const payload: JwtPayload = user;
    const accessToken = await this._jwtService.sign(payload);

    return { accessToken };
  }

  async updateUsuarioRol(idUsuario: number, usuarioActualizarRolDto: UsuarioActualizarRolDto): Promise<Usuario> {
    return this._repositoryUsuario.updateUsuarioRol(idUsuario, usuarioActualizarRolDto);
  }

  async updateUsuarioPassword(idUsuario: number, usuarioActualizarPasswordDto: UsuarioActualizarPasswordDto): Promise<{ accessToken: string }> {

    const user = await this._repositoryUsuario.updateUsuarioPassword(idUsuario, usuarioActualizarPasswordDto);

    if (!user) {
      throw new UnauthorizedException('Credenciales inválidas.');
    }

    const payload: JwtPayload = user;
    const accessToken = await this._jwtService.sign(payload);

    return { accessToken };
  }
}
