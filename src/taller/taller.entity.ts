import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, Unique } from 'typeorm';
import { Usuario } from '../usuario/usuario.entity';
import { HistoricoTaller } from '../historico-taller/historico-taller.entity';
import { RegistroTaller } from '../registro-taller/registro-taller.entity';
import { TipoTaller } from '../tipo-taller/tipo-taller.entity';
import { Etapa } from '../etapa/etapa.entity';

@Entity()
//@Unique(['nombreTaller'])
export class Taller extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombreTaller: string;

  @Column()
  descripcion: string;

  @Column({ default: 'No iniciado' })
  estado: string;

  @Column({ default: '0:00' })
  duracion: string;

  @Column()
  numeroParticipantes: number;

  @Column()
  numeroRegistrados: number;

  @Column()
  fechaRealizacion: Date;

  @ManyToOne(type => Usuario, usuario => usuario.talleres, { cascade: true, onDelete: 'SET NULL' })
  @JoinColumn({ name: 'fkUsuario' })
  encargado: Usuario;

  @ManyToOne(type => Etapa, etapa => etapa.talleres, { cascade: true, onDelete: 'SET NULL' })
  @JoinColumn({ name: 'fkEtapa' })
  etapa: Etapa;

  @OneToMany(type => RegistroTaller, registro => registro.taller)
  registros: RegistroTaller[];

  @ManyToOne(type => TipoTaller, tipo => tipo.talleres, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkTipoTaller' })
  tipo: TipoTaller;
}
