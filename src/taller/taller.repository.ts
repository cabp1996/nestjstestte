import { TallerActualizarEstadoDto } from './DTO/taller-actualizar-estado.dto';
import { Repository, EntityRepository } from 'typeorm';
import { Taller } from './taller.entity';
import { NotFoundException, ConflictException, InternalServerErrorException } from '@nestjs/common';
import { TallerCrearDto } from './DTO/taller-crear.dto';
import { TallerActualizarDto } from './DTO/taller-actualizar.dto';
import { Usuario } from '../usuario/usuario.entity';
import { Etapa } from 'src/etapa/etapa.entity';

@EntityRepository(Taller)
export class TallerRepository extends Repository<Taller>{

  async getTalleres(usuario: Usuario): Promise<Taller[]> {

    /*const talleres = this.find({
      join: {
        alias: "taller",
        leftJoinAndSelect: {
          encargado: "taller.encargado",
          registros: "taller.registros"
        }
      },
    });*/

    const talleres = await this.find({
      relations: ['encargado', 'encargado.rol', 'registros', 'registros.estudiante', 'tipo'],
      where: { encargado: usuario }
    });

    if (!talleres) {
      throw new NotFoundException("No se hallaron talleres.");
    }

    talleres.forEach(
      (taller) => {
        if (taller.encargado) {
          delete taller.encargado.password;
          delete taller.encargado.sal;
        }
      }
    )

    return talleres;
  }


  async getTallerByID(id: number): Promise<Taller> {
    const taller = await this.findOne(id, {
      join: {
        alias: "taller",
        leftJoinAndSelect: {
          encargado: "taller.encargado",
          etapa: "taller.etapa",
		  tipo:"taller.tipo"
        }
      }
    });

    if (!taller) {
      throw new NotFoundException("No se halló ese taller.");
    }

    if (taller.encargado) {
      delete taller.encargado.password;
      delete taller.encargado.sal;
    }


    return taller;
  }

  async createTaller(tallerCrearDto: TallerCrearDto, usuario: Usuario): Promise<Taller> {

    const { nombreTaller, descripcion, fechaRealizacion, numeroParticipantes, tipo } = tallerCrearDto;

    delete usuario.sal;
    delete usuario.password;

    const nuevoTaller = new Taller();
    const etapa = new Etapa();
    etapa.id = 1;

    nuevoTaller.nombreTaller = nombreTaller;
    nuevoTaller.descripcion = descripcion;
    nuevoTaller.fechaRealizacion = fechaRealizacion;
    nuevoTaller.numeroParticipantes = numeroParticipantes;
	nuevoTaller.numeroRegistrados=0;
    nuevoTaller.encargado = usuario;
    nuevoTaller.tipo = tipo;
    nuevoTaller.etapa = etapa;

    try {
      await nuevoTaller.save();
    } catch (error) {
	
      /*if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe un taller con ese nombre.');
      } else {*/
        throw new InternalServerErrorException();
      //}
    }



    return nuevoTaller;
  }


  async updateTaller(idTaller: number, tallerActualizarDto: TallerActualizarDto): Promise<Taller> {

    const { nombreTaller, descripcion, fechaRealizacion, numeroParticipantes, tipo } = tallerActualizarDto;


    const tallerActualizar = await this.getTallerByID(idTaller);
    tallerActualizar.nombreTaller = nombreTaller;
    tallerActualizar.descripcion = descripcion;
    tallerActualizar.fechaRealizacion = fechaRealizacion;
    tallerActualizar.numeroParticipantes = numeroParticipantes;
    tallerActualizar.tipo = tipo;

    try {
      await tallerActualizar.save();
    } catch (error) {

      //console.log(error)

      /*if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe un taller con ese nombre.');
      } else {*/
        throw new InternalServerErrorException();
      //}
    }



    return tallerActualizar;
  }

  async updateTallerEstado(idTaller: number, actualizarEstadoTallerDto: TallerActualizarEstadoDto): Promise<void> {

    const { estado, duracion, etapa } = actualizarEstadoTallerDto;
    const tallerActualizar = await this.getTallerByID(idTaller);
    tallerActualizar.estado = estado;
    tallerActualizar.duracion = duracion;
    tallerActualizar.etapa = etapa;
    await tallerActualizar.save();

  }

  async updateTallerEstadoEtapa(idTaller: number, etapa: Etapa): Promise<void> {
    const tallerActualizar = await this.getTallerByID(idTaller);
    tallerActualizar.etapa = etapa;
    await tallerActualizar.save();
  }

  async updateTallerParticipantes(idTaller: number): Promise<Taller> {
    
    const tallerActualizar = await this.getTallerByID(idTaller);
    const response=await this.query('select count(*) as total from taller inner join registro_taller on taller.id=registro_taller.fkTaller where taller.id='+idTaller+';');
    let nuevoNumero:number=Number(response[0].total);
    tallerActualizar.numeroRegistrados = nuevoNumero;

    try {
      await tallerActualizar.save();
    } catch (error) {
      console.log(error)
    }
   
    return tallerActualizar;
  }

}
