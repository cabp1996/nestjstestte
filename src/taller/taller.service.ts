import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TallerRepository } from './taller.repository';
import { Taller } from './taller.entity';
import { TallerCrearDto } from './DTO/taller-crear.dto';
import { TallerActualizarDto } from './DTO/taller-actualizar.dto';
import { Usuario } from '../usuario/usuario.entity';
import { TallerActualizarEstadoDto } from './DTO/taller-actualizar-estado.dto';
import { Etapa } from '../etapa/etapa.entity';

@Injectable()
export class TallerService {

  constructor(@InjectRepository(TallerRepository) private _repositoryTaller: TallerRepository) {
  }

  async getTalleres(usuario: Usuario): Promise<Taller[]> {
    return this._repositoryTaller.getTalleres(usuario);
  }

  async getTallerByID(id: number): Promise<Taller> {
    return this._repositoryTaller.getTallerByID(id);
  }

  async createTaller(tallerCrearDto: TallerCrearDto, usuario: Usuario): Promise<Taller> {

    return this._repositoryTaller.createTaller(tallerCrearDto, usuario);
  }

  async updateTaller(idTaller: number, tallerActualizarDto: TallerActualizarDto): Promise<Taller> {
    return this._repositoryTaller.updateTaller(idTaller, tallerActualizarDto);
  }

  async updateTallerEstado(idTaller: number, actualizarEstadoTallerDto: TallerActualizarEstadoDto): Promise<void> {
    this._repositoryTaller.updateTallerEstado(idTaller, actualizarEstadoTallerDto);
  }

  async updateTallerEstadoEtapa(idTaller: number, etapa: Etapa): Promise<void> {
    this._repositoryTaller.updateTallerEstadoEtapa(idTaller, etapa);
  }

   updateTallerParticipantes(idTaller: number): Promise<Taller> {
    return  this._repositoryTaller.updateTallerParticipantes(idTaller);
  }

  async deleteTaller(idTaller: number): Promise<void> {

    const resultado = await this._repositoryTaller.delete(idTaller);

    if (resultado.affected === 0) {
      throw new NotFoundException("No existe un taller con el id: " + idTaller);
    }

  }

}

