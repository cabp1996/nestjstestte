import { Controller, Get, Post, Body, Param, ParseIntPipe, Patch, Delete, UseGuards, ValidationPipe } from '@nestjs/common';
import { TallerService } from './taller.service';
import { Taller } from './taller.entity';
import { TallerCrearDto } from './DTO/taller-crear.dto';
import { TallerActualizarDto } from './DTO/taller-actualizar.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../usuario/usuario.obtener.decorator';
import { Usuario } from '../usuario/usuario.entity';
import { TallerActualizarEstadoDto } from './DTO/taller-actualizar-estado.dto';
import { Etapa } from '../etapa/etapa.entity';

@Controller('taller')
@UseGuards(AuthGuard('jwt'))
export class TallerController {

  constructor(private readonly _servicioTaller: TallerService) {

  }

  @Get()
  getTalleres(@GetUser() usuario: Usuario): Promise<Taller[]> {
    return this._servicioTaller.getTalleres(usuario);
  }

  @Get('/:id')
  getTallerByID(@Param('id', ParseIntPipe) id: number): Promise<Taller> {
    return this._servicioTaller.getTallerByID(id);
  }

  @Post()
  createTaller(@Body(ValidationPipe) tallerCrearDto: TallerCrearDto, @GetUser() usuario: Usuario): Promise<Taller> {

    return this._servicioTaller.createTaller(tallerCrearDto, usuario);
  }

  @Patch('/:id')
  updateTaller(@Param('id', ParseIntPipe) id: number, @Body(ValidationPipe) tallerActualizarDto: TallerActualizarDto): Promise<Taller> {
    return this._servicioTaller.updateTaller(id, tallerActualizarDto);
  }

  @Patch('estado/:id')
  updateTallerEstado(@Param('id') idTaller: number, @Body(ValidationPipe) actualizarEstadoTallerDto: TallerActualizarEstadoDto): Promise<void> {
    return this._servicioTaller.updateTallerEstado(idTaller, actualizarEstadoTallerDto);
  }

  @Patch('estado/:id/etapa')
  updateTallerEstadoEtapa(@Param('id') idTaller: number, @Body() etapa: Etapa): Promise<void> {
    return this._servicioTaller.updateTallerEstadoEtapa(idTaller, etapa);
  }

  @Delete('/:id')
  deleteTaller(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this._servicioTaller.deleteTaller(id);
  }

}
