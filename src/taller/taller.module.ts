import { Module } from '@nestjs/common';
import { TallerController } from './taller.controller';
import { TallerService } from './taller.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TallerRepository } from './taller.repository';

@Module({
  imports: [TypeOrmModule.forFeature([TallerRepository])],
  controllers: [TallerController],
  providers: [TallerService],
  exports: [TallerService]
})
export class TallerModule { }
