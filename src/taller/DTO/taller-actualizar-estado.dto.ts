import { IsString } from "class-validator";
import { Etapa } from '../../etapa/etapa.entity';


export class TallerActualizarEstadoDto {

    @IsString()
    estado: string;

    @IsString()
    duracion: string;

    etapa: Etapa;

}