import { Usuario } from "../../usuario/usuario.entity";
import { TipoTaller } from '../../tipo-taller/tipo-taller.entity';
import { IsString, MinLength, MaxLength, IsInt } from "class-validator";

export class TallerCrearDto{

  @IsString()
  @MinLength(5,{message:'El nombre del taller debe tener un tamaño mínimo de 5 caracteres.'})
  @MaxLength(50,{message:'El nombre del taller debe tener un tamaño máximo de 50 caracteres.'})
  nombreTaller:string;

  @IsString()
  @MaxLength(150,{message:'El nombre del taller debe tener un tamaño máximo de 150 caracteres.'})
  descripcion:string;

  tipo:TipoTaller;

  @IsInt({message:'El número de participantes del taller debe ser un número entero.'})
  numeroParticipantes:number;

  fechaRealizacion:Date;

}
