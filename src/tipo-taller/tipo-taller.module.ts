import { Module } from '@nestjs/common';
import { TipoTallerService } from './tipo-taller.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoTallerRespository } from './tipo-taller.repository';
import { TipoTallerController } from './tipo-taller.controller';

@Module({
  providers: [TipoTallerService],
  imports: [TypeOrmModule.forFeature([TipoTallerRespository])],
  controllers: [TipoTallerController],
})
export class TipoTallerModule { }
