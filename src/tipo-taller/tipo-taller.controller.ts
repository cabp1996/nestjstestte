import { Controller, Get, UseGuards } from '@nestjs/common';
import { TipoTallerService } from './tipo-taller.service';
import { TipoTaller } from './tipo-taller.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('tipo-taller')
export class TipoTallerController {

  constructor(private _servicioTipoTaller: TipoTallerService) {

  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  getTiposTaller(): Promise<TipoTaller[]> {
    return this._servicioTipoTaller.getTiposTaller();
  }

  @Get('cargar')
  cargarData(): Promise<void> {
    this._servicioTipoTaller.cargarData();
    return;
  }
}
