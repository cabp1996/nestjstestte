import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Taller } from '../taller/taller.entity';

@Entity()
export class TipoTaller extends BaseEntity{

  @PrimaryGeneratedColumn()
  id:number;

  @Column()
  nombre:string;

  @OneToMany(type => Taller, taller => taller.tipo)
  talleres: Taller[]
}
