import { Repository, EntityRepository } from "typeorm";

import { InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { TipoTaller } from './tipo-taller.entity';
import { TipoTallerCrearDto } from './DTO/tipo-taller-crear.dto';

@EntityRepository(TipoTaller)
export class TipoTallerRespository extends Repository<TipoTaller>{

  getTiposTalleres(): Promise<TipoTaller[]> {
    const tiposTalleres = this.find();

    if (!tiposTalleres) {
      throw new NotFoundException('No se ha podido recuperar los tipos de taller.');
    }
    return tiposTalleres;
  }


  async guardarTipoTaller(tipoTallerCrearDto: TipoTallerCrearDto): Promise<TipoTaller> {

    const { id, nombre } = tipoTallerCrearDto;

    const nuevoTipo = new TipoTaller();
    nuevoTipo.nombre = nombre;
    nuevoTipo.id = id;

    try {
      await nuevoTipo.save();
    } catch (error) {
      throw new InternalServerErrorException();
    }

    return nuevoTipo;

  }
}
