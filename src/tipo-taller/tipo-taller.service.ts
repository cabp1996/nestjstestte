import { Injectable } from '@nestjs/common';
import { TipoTallerRespository } from './tipo-taller.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { TipoTaller } from './tipo-taller.entity';
import { TipoTallerCrearDto } from './DTO/tipo-taller-crear.dto';

@Injectable()
export class TipoTallerService {

  constructor(@InjectRepository(TipoTallerRespository) private _repositoryTipoTaller: TipoTallerRespository) {

  }

  cargarData() {
    this.revisarTipos().then(
      (tipos: TipoTaller[]) => {
        if (tipos) {
          if (tipos.length == 0) {
            const tipo1 = new TipoTallerCrearDto(1, 'Ciencias');
            const tipo2 = new TipoTallerCrearDto(2, 'Matemáticas');
            const tipo3 = new TipoTallerCrearDto(3, 'Biología');
            const tipo4 = new TipoTallerCrearDto(4, 'Otros');

            this._repositoryTipoTaller.guardarTipoTaller(tipo1);
            this._repositoryTipoTaller.guardarTipoTaller(tipo2);
            this._repositoryTipoTaller.guardarTipoTaller(tipo3);
            this._repositoryTipoTaller.guardarTipoTaller(tipo4);

          }
        }
      });
  }

  getTiposTaller(): Promise<TipoTaller[]> {
    return this._repositoryTipoTaller.getTiposTalleres();
  }

  async revisarTipos(): Promise<TipoTaller[]> {
    return this._repositoryTipoTaller.find();
  }

}
