import { Repository, EntityRepository } from "typeorm";
import { General } from "./general.entity";
import { NotFoundException, InternalServerErrorException } from "@nestjs/common";
import { GeneralGuardarDto } from './DTO/general-guardar.dto';

@EntityRepository(General)
export class GeneralRepository extends Repository<General>{

    async obtenerGeneral(): Promise<General> {
        const general = await this.findOne(1);

        if (!general) {
            throw new NotFoundException("No se ha podido recuperar las credenciales de azure o no se han guardado todavía por primera vez. Registrarlas del ser el caso.");
        }
        return general;
    }

    async guardarGeneral(guardarGeneralDto: GeneralGuardarDto): Promise<General> {

        const { key, endpoint } = guardarGeneralDto;

        try {
            const general = await this.findOne(1);

            if (general) {

                general.endpoint = endpoint;
                general.key = key;
                await general.save();
                return general;

            } else {

                const nuevoGeneral = new General();
                nuevoGeneral.key = key;
                nuevoGeneral.endpoint = endpoint;
                await nuevoGeneral.save();
                return nuevoGeneral;
            }

        } catch (error) {
            throw new InternalServerErrorException();
        }
    }

}
