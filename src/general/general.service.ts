import { GeneralGuardarDto } from './DTO/general-guardar.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { General } from './general.entity';
import { GeneralRepository } from './general.repository';

@Injectable()
export class GeneralService {

    constructor(@InjectRepository(GeneralRepository) private _repositoryGeneral: GeneralRepository) {

    }

    async obtenerGeneral(): Promise<General> {
        return this._repositoryGeneral.obtenerGeneral();
    }

    async guardarGeneral(generalCrearDto: GeneralGuardarDto): Promise<General> {
        return this._repositoryGeneral.guardarGeneral(generalCrearDto);
    }

}
