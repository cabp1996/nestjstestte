import { GeneralService } from './general.service';
import { Body, Controller, Get, Post } from '@nestjs/common';
import { General } from './general.entity';
import { GeneralGuardarDto } from './DTO/general-guardar.dto';

@Controller('general')
export class GeneralController {

    constructor(private readonly _servicioGeneral: GeneralService) {
    }

    @Get()
    obtenerGeneral(): Promise<General> {
        return this._servicioGeneral.obtenerGeneral();
    }

    @Post()
    guardarGeneral(@Body() generalGuardarDto: GeneralGuardarDto): Promise<General> {
        return this._servicioGeneral.guardarGeneral(generalGuardarDto);
    }
}
