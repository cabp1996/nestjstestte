import { GeneralRepository } from './general.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GeneralController } from './general.controller';
import { GeneralService } from './general.service';

@Module({
  imports: [TypeOrmModule.forFeature([GeneralRepository])],
  controllers: [GeneralController],
  providers: [GeneralService],
  exports: [GeneralService]
})
export class GeneralModule { }
