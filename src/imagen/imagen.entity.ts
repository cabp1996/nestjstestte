import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Estudiante } from "../estudiante/estudiante.entity";

@Entity()
export class Imagen extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  urlImagen: string;

  @ManyToOne(type => Estudiante, estudiante => estudiante.imagenes, { cascade: true, onDelete: 'CASCADE' })
  estudiante: Estudiante;
}
