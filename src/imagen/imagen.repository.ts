import { Repository, BaseEntity, EntityRepository } from "typeorm";
import { Imagen } from "./imagen.entity";
import { NotFoundException } from "@nestjs/common";
import { ImagenInsertarDto } from './DTO/imagen-insertar.dto';

@EntityRepository(Imagen)
export class ImagenRepository extends Repository<Imagen>{

  async insertarImagenes(imagenInsertarDto: ImagenInsertarDto) {

    const { estudiante, urlSImagen } = imagenInsertarDto;

    let response: boolean = true;

    urlSImagen.forEach(async (url) => {

      try {

        const nuevaImagen = new Imagen();
        nuevaImagen.urlImagen = url;
        nuevaImagen.estudiante = estudiante;

        await nuevaImagen.save();

      } catch (error) {
        response = false;
      }

    });

    return response;
  }
}
