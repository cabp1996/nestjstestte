import { Module } from '@nestjs/common';
import {ImagenService} from './imagen.service';
import { ImagenController } from './imagen.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImagenRepository } from './imagen.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ImagenRepository])],
  controllers: [ImagenController],
  providers: [ImagenService]
})
export class ImagenModule { }
