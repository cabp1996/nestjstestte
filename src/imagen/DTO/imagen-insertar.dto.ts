import { Estudiante } from '../../estudiante/estudiante.entity';

export class ImagenInsertarDto{
  urlSImagen:string[];
  estudiante:Estudiante;
}
