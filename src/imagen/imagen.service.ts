import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ImagenRepository } from './imagen.repository';
import { ImagenInsertarDto } from './DTO/imagen-insertar.dto';

@Injectable()
export class ImagenService {

  constructor(@InjectRepository(ImagenRepository) private _repositoryImagen: ImagenRepository) {
  }

  obtenerImagenes(){

  }

  insertarImagenes(imagenInsertarDto:ImagenInsertarDto){
    return this._repositoryImagen.insertarImagenes(imagenInsertarDto);
  }

  async borrarImagen(idImagen: number): Promise<void> {

    const resultado = await this._repositoryImagen.delete(idImagen);

    if (resultado.affected === 0) {
      throw new NotFoundException("No existe una imagen con el id: " + idImagen);
    }

  }

}
