import { Controller, Post, Body, Delete, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { ImagenService } from './imagen.service';
import { ImagenInsertarDto } from './DTO/imagen-insertar.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('imagen')
@UseGuards(AuthGuard('jwt'))
export class ImagenController {

  constructor(private readonly _servicioImagen: ImagenService) { }

  @Post()
  insertarImagenes(@Body() insertarImagenDto: ImagenInsertarDto) {
    return this._servicioImagen.insertarImagenes(insertarImagenDto);
  }

  @Delete('/:id')
  deletePersona(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this._servicioImagen.borrarImagen(id);
  }

}
