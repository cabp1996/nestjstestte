import { HttpService, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { CargarModeloDto } from './DTOs/cargar-modelo.dto';
import { AnalisEmocionDto } from './DTOs/analisis-emocion-dto';
import { BorrarModeloDto } from './DTOs/borrar-modelo.dto';
import { RegistroTallerService } from '../registro-taller/registro-taller.service';
import { HistoricoTallerService } from '../historico-taller/historico-taller.service';
import { HistoricoTallerCrearDto } from '../historico-taller/DTO/historico-taller-crear.dto';
import { RegistroTaller } from '../registro-taller/registro-taller.entity';
import { Etapa } from '../etapa/etapa.entity';
import { GeneralService } from '../general/general.service';
import { General } from '../general/general.entity';

@Injectable()
export class FaceService {

    _baseURL: string = '';
    _subscriptionKey: string = '';

    constructor(private httpService: HttpService,
        private _servicioRegistroTaller: RegistroTallerService,
        private _servicioHistoricoTaller: HistoricoTallerService,
        private _servicioGeneral: GeneralService) { }


    guardarHistoricos(resultados: any[], etapa: Etapa, tiempoCronometroDate: string, url: string) {

        let historicosEmocionesTallerDto: HistoricoTallerCrearDto[] = [];
        resultados.forEach((resultado) => {

            const registroTallerActual = new RegistroTaller();
            registroTallerActual.id = Number(resultado.idRegistro);

            const nuevoHistoricoDto = new HistoricoTallerCrearDto();
            nuevoHistoricoDto.registroTaller = registroTallerActual;
            nuevoHistoricoDto.registro = resultado.data;
            nuevoHistoricoDto.etapa = etapa;
            nuevoHistoricoDto.observaciones = resultado.observaciones ? resultado.observaciones : '';
            nuevoHistoricoDto.fuente = 1;
            nuevoHistoricoDto.tiempo = tiempoCronometroDate;
            nuevoHistoricoDto.url = url;
            historicosEmocionesTallerDto.push(nuevoHistoricoDto);
        });

        this._servicioHistoricoTaller.guardarHistoricosTaller(historicosEmocionesTallerDto);

    }


    async cargarModelo(cargarModeloDto: CargarModeloDto) {

        try {

            if (this._baseURL === '' || this._subscriptionKey === '') {

                let generalKeys = await this._servicioGeneral.obtenerGeneral();

                if (generalKeys) {
                    this._baseURL = generalKeys.endpoint;
                    this._subscriptionKey = generalKeys.key;
                } else {
                    return;
                }

            }


            //const subscriptionKey = 'c4c8c8443d3047db804c5591956fa396';
            //const baseUrl = 'https://facetesistest.cognitiveservices.azure.com';

            const { idTaller, nombreGrupo } = cargarModeloDto;

            const participantes = await this._servicioRegistroTaller.obtenerRegistrosSegunTallerId(idTaller);

            let personasParticipantes: any[] = [];

            participantes.forEach((registroParticipante) => {

                let urls: string[] = [];
                registroParticipante.estudiante.imagenes.forEach((img) => urls.push(img.urlImagen));

                personasParticipantes.push({
                    idRegistro: registroParticipante.id,
                    pictures: urls
                });

            });

            const personGroupId = nombreGrupo;



            await this.createPersonGroup(this._baseURL, this._subscriptionKey, personGroupId);

            for (let person of personasParticipantes) {
                const response = await this.createPerson(this._baseURL, this._subscriptionKey, personGroupId, person.idRegistro);
                for (let picture of person.pictures) {
                    await this.addImageToPerson(this._baseURL, this._subscriptionKey, personGroupId, response.personId, picture)
                }
            }

            this.trainPersonGroup(this._baseURL, this._subscriptionKey, personGroupId);

            let finishedTraining = false

            while (!finishedTraining) {
                await this.waitFor(1000);
                const response = await this.getTrainingStatus(this._baseURL, this._subscriptionKey, personGroupId);
                if (response.status === 'succeeded') finishedTraining = true;
            }

            return finishedTraining;

        } catch (e) {
            return false;
        }
    }


    async detectarEmociones(analisEmocionDto: AnalisEmocionDto) {

        try {

            if (this._baseURL === '' || this._subscriptionKey === '') {

                const generalKeys: General = await this._servicioGeneral.obtenerGeneral();
                if (generalKeys) {
                    this._baseURL = generalKeys.endpoint;
                    this._subscriptionKey = generalKeys.key;
                } else {
                    return;
                }
            }

            const { urlImg, nombreGrupo, etapa, tiempoCronometroDate } = analisEmocionDto;


            let resultadoFinal: any[] = [];

            //hace un reconocimiento de rostros con sus características y les da un FACEID1
            const resultadoReconocimientoRostros: any[] = await this.detectarRostrosyEmociones(this._baseURL, this._subscriptionKey, urlImg);

            //se envía una foto y de un grupo determinado, trae el face Id(es un FACEID1 igual al reconocido en el rostros en el paso anterior) y el posible candidato identificado(el id de la persona, a consultarse en 'personas' y la confianza)
            let resultadoCandidatos: any[] = await this.identificarPersonaSegunRostro(this._baseURL, this._subscriptionKey, nombreGrupo, resultadoReconocimientoRostros);
            resultadoCandidatos = resultadoCandidatos.filter((res) => res.candidates && res.candidates.length);
            //console.log('resultadoCandidatos', resultadoCandidatos)

            if (resultadoCandidatos.length === 0) {
                return resultadoFinal;
            }

            //obtengo los nombres y los person id de los miembros de un grupo determinado
            let personas: any[] = await this.obtenerNombreSegunFaceId(this._baseURL, this._subscriptionKey, nombreGrupo);
            //console.log('personas', personas)


            resultadoCandidatos.forEach((candidato) => {

                const candidatoId = candidato.candidates[0].personId;
                personas.forEach((persona, index) => {
                    if (persona.personId === candidatoId) {

                        //Comparo los FACEID1 después de haber identificado la persona
                        let indexFace: number = -1;
                        const resultadoParcial = resultadoReconocimientoRostros.find(
                            (res, indexFaceEmotion) => {
                                if (res.faceId === candidato.faceId) {
                                    indexFace = indexFaceEmotion;
                                    return res;
                                }
                            });

                        if (resultadoParcial) {

                            let datosEmocionPivot: any = resultadoParcial.faceAttributes.emotion;
                            let nuevoRegistroEmociones = {
                                happy: datosEmocionPivot['happiness'],
                                sad: datosEmocionPivot['sadness'],
                                surprise: datosEmocionPivot['surprise'],
                                neutral: datosEmocionPivot['neutral'],
                                angry: datosEmocionPivot['anger'],
                                disgust: datosEmocionPivot['disgust'],
                                fear: datosEmocionPivot['fear']
                            };
                            resultadoFinal.push({
                                data: JSON.stringify(nuevoRegistroEmociones),
                                idRegistro: persona.name,
                                observaciones: JSON.stringify(resultadoParcial)
                            });

                            resultadoReconocimientoRostros.splice(indexFace, 1);
                        }
                        personas.splice(index, 1);

                        // return resultadoFinal;
                    }
                })
            });

            this.guardarHistoricos(resultadoFinal, etapa, tiempoCronometroDate,urlImg);

            resultadoFinal.forEach((res) => delete res.observaciones)
            return resultadoFinal;

        } catch (e) {
            throw new NotFoundException('No se ha podido realizar la detección');
        }

    }

    async identificarPersonaSegunRostro(baseURL: string, subscriptionKey: String, groupId: string, resultadoReconocimientoRostros: any[]) {

        let faceIds: string[] = [];
        resultadoReconocimientoRostros.forEach((resultado) => {
            faceIds.push(resultado.faceId);
        });

        let body = {
            "personGroupId": groupId,
            "faceIds": faceIds,
            "maxNumOfCandidatesReturned": 1,
            "confidenceThreshold": 0.5
        };

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey }


        try {
            const datos = await this.httpService.post(`${baseURL}/face/v1.0/identify`, body,
                { headers: headers }).toPromise();

            return datos.data;
        } catch (e) {
            return e;
        }

    }

    async detectarRostrosyEmociones(baseURL: string,
        subscriptionKey: string,
        url: string) {

        let params = {
            'returnFaceId': 'true',
            'returnRecognitionModel': true,
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender,facialHair,glasses,emotion,hair,makeup,occlusion,accessories',
        }

        let body = {
            url: url
        };

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey }


        const datos = await this.httpService.post(`${baseURL}/face/v1.0/detect`, body,
            { headers: headers, params: params }).toPromise();

        return datos.data;
    }


    async obtenerNombreSegunFaceId(baseURL: string,
        subscriptionKey: string,
        personGroupId: string) {

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey }

        const response = await this.httpService.get(`${baseURL}/face/v1.0/persongroups/${personGroupId}/persons`,
            { headers: headers }).toPromise();

        return response.data;
    }



    waitFor = (time) => {
        return new Promise((resolve, reject) => {
            return setTimeout(resolve, time)
        })
    }

    async createPersonGroup(
        baseURL: string,
        subscriptionKey: string,
        personGroupId: string) {

        let body = {
            name: personGroupId,
            userData: 'prueba',
            recognitionModel: 'recognition_01'
        }

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey, 'Content-Type': 'application/json' }

        const response = await this.httpService.put(`${baseURL}/face/v1.0/persongroups/${personGroupId}`, body,
            { headers: headers }).toPromise();
        return response.data;
    }

    async createPerson(baseURL: string,
        subscriptionKey: string,
        personGroupId: string,
        personId: string) {


        let body = {
            name: personId,
            userData: 'datoPersonaId'
        }
        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey }

        const response = await this.httpService.post(`${baseURL}/face/v1.0/persongroups/${personGroupId}/persons`, body,
            { headers: headers }).toPromise();
        return response.data;
    }

    async addImageToPerson(baseURL: string,
        subscriptionKey: string,
        personGroupId: string,
        personId: string,
        faceUrl: string) {


        let body = {
            url: faceUrl
        }
        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey };


        const response = await this.httpService.post(`${baseURL}/face/v1.0/persongroups/${personGroupId}/persons/${personId}/persistedFaces`, body,
            { headers: headers }).toPromise();

        return response.data;

    }

    trainPersonGroup(
        baseURL: string,
        subscriptionKey: string,
        personGroupId: string) {

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey }
        let body = {

        }
        this.httpService.post(`${baseURL}/face/v1.0/persongroups/${personGroupId}/train`, body,
            { headers: headers }).toPromise();

        //'https://ec.cognitiveservices.azure.com/face/v1.0/persongroups/testnode/train'

    }

    async getTrainingStatus(baseURL: string,
        subscriptionKey: string,
        personGroupId: string) {

        let headers = { 'Ocp-Apim-Subscription-Key': subscriptionKey };


        try {
            const response = await this.httpService.get(`${baseURL}/face/v1.0/persongroups/${personGroupId}/training`,
                { headers: headers }).toPromise();

            return response.data;
        } catch (e) {
            console.log('error')
            return e;
        }


    }

    getPersonGroup() {
        //https://{endpoint}/face/v1.0/persongroups/{personGroupId}[?returnRecognitionModel]
        const personGroupId = 'testnode';
        let headers = { 'Ocp-Apim-Subscription-Key': 'b783961c8b3349fab6d90130ab863f6f' }
        return this.httpService.get(`https://ec.cognitiveservices.azure.com/face/v1.0/persongroups/${personGroupId}?returnRecognitionModel=true`,
            { headers: headers }).pipe(
                map(response => {
                    return response.data
                }),
            );

    }



    async borrarPersonGroup(borrarModeloDto: BorrarModeloDto) {
        try {

            if (this._baseURL === '' || this._subscriptionKey === '') {
                const generalKeys: General = await this._servicioGeneral.obtenerGeneral();
                if (generalKeys) {
                    this._baseURL = generalKeys.endpoint;
                    this._subscriptionKey = generalKeys.key;
                } else {
                    return;
                }
            }

            const { nombreGrupo } = borrarModeloDto;
            const personGroupId = nombreGrupo;

            let headers = { 'Ocp-Apim-Subscription-Key': this._subscriptionKey }

            return this.httpService.delete(`${this._baseURL}/face/v1.0/persongroups/${personGroupId}`,
                { headers: headers }).pipe(
                    map(response => {
                        this._subscriptionKey = '';
                        this._baseURL = '';
                        return response.status;
                    })
                );

        } catch (e) {
            throw new InternalServerErrorException();
        }

    }


}




