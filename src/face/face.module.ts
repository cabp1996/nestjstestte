import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegistroTallerService } from '../registro-taller/registro-taller.service';
import { RegistroTallerRepository } from '../registro-taller/registro-taller.repository';
import { FaceController } from './face.controller';
import { FaceService } from './face.service';
import { TallerModule } from '../taller/taller.module';
import { HistoricoTallerRepository } from '../historico-taller/historico-taller.repository';
import { HistoricoTallerService } from '../historico-taller/historico-taller.service';
import { GeneralService } from '../general/general.service';
import { GeneralRepository } from '../general/general.repository';

@Module({
    imports: [HttpModule, TypeOrmModule.forFeature([RegistroTallerRepository, HistoricoTallerRepository, GeneralRepository]), TallerModule],
    providers: [FaceService, RegistroTallerService, HistoricoTallerService, GeneralService],
    controllers: [FaceController],
})
export class FaceModule { }
