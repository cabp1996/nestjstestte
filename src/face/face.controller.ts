import { FaceService } from './face.service';
import { Body, Controller, Get, Post, ValidationPipe, Response, UseGuards } from '@nestjs/common';
import { CargarModeloDto } from './DTOs/cargar-modelo.dto';
import { AnalisEmocionDto } from './DTOs/analisis-emocion-dto';
import { BorrarModeloDto } from './DTOs/borrar-modelo.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('face')
@UseGuards(AuthGuard('jwt'))
export class FaceController {

    constructor(private _faceService: FaceService) {
    }

    @Post('detectarEmociones')
    async obtenerEmociones(@Body() analisEmocionDto: AnalisEmocionDto, @Response() response) {

        const resultados = await this._faceService.detectarEmociones(analisEmocionDto);

        if (resultados.length === 0) {
            return response.status(401).send(
                {
                    Error: 'No se ha reconocido ningun rostro en la imagen.',
                    error: 400
                });
        }

        return response.status(200).send(resultados);
    }

    @Post('cargarModelo')
    async cargarModelo(@Body() cargarModeloDto: CargarModeloDto, @Response() response) {
        const resultado: boolean = await this._faceService.cargarModelo(cargarModeloDto);

        if (resultado === false) {
            return response.status(401).send(
                {
                    Error: 'Ha ocurrido un error',
                });
        }

        return response.status(200).send(
            {
                Success: resultado,
            });
    }

    @Post('borrarModelo')
    async borrarPersonGroup(@Body() borrarModeloDto: BorrarModeloDto) {

        const resultado = await this._faceService.borrarPersonGroup(borrarModeloDto);
        return resultado;
    }

}
