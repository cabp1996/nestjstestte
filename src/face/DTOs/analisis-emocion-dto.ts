import { Etapa } from "../../etapa/etapa.entity";

export class AnalisEmocionDto {
    urlImg: string;
    tiempoCronometroDate: string;
    nombreGrupo: string;
    etapa: Etapa;
}