import { AppController } from './app.controller';
import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { RolModule } from './rol/rol.module';
import { UsuarioModule } from './usuario/usuario.module';
import { PersonaModule } from './persona/persona.module';
import { TallerModule } from './taller/taller.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { EtapaModule } from './etapa/etapa.module';
import { EstudianteModule } from './estudiante/estudiante.module';
import { HistoricoTallerModule } from './historico-taller/historico-taller.module';
import { RegistroTallerModule } from './registro-taller/registro-taller.module';
import { GradoModule } from './grado/grado.module';
import { TipoTallerModule } from './tipo-taller/tipo-taller.module';
import { ImagenModule } from './imagen/imagen.module';
import { FaceModule } from './face/face.module';
import { GeneralModule } from './general/general.module';

@Module({
  imports: [
    RolModule,
    UsuarioModule,
    PersonaModule,
    TallerModule,
    TypeOrmModule.forRoot(
      typeOrmConfig
    ),
    EtapaModule,
    EstudianteModule,
    HistoricoTallerModule,
    RegistroTallerModule,
    GradoModule,
    TipoTallerModule,
    ImagenModule,
    FaceModule,
    GeneralModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
