import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, Unique } from 'typeorm';

@Entity()
@Unique(['identificacion'])
export class Persona extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombres: string;

  @Column({ nullable: true })
  identificacion: string;

  @Column()
  genero: string;

  @Column()
  edad: number;

  @Column()
  esEstudiante: boolean;


}
