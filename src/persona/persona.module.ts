import { Module } from '@nestjs/common';
import { PersonaController } from './persona.controller';
import { PersonaService } from './persona.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonaRepository } from './persona.repository';
import { TallerService } from '../taller/taller.service';
import { TallerRepository } from '../taller/taller.repository';

@Module({
  imports:[TypeOrmModule.forFeature([PersonaRepository,TallerRepository])],
  controllers: [PersonaController],
  providers: [PersonaService,TallerService]
})
export class PersonaModule {}
