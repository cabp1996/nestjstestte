import { Repository, EntityRepository } from "typeorm";
import { Persona } from './persona.entity';
import { NotFoundException, InternalServerErrorException, ConflictException } from "@nestjs/common";
import { PersonaAgregarDto } from "./DTO/persona-agregar.dto";
import { PersonaActualizarDto } from "./DTO/persona-actualizar.dto";

@EntityRepository(Persona)
export class PersonaRepository extends Repository<Persona>{

  async getPersonasByID(id: number): Promise<Persona> {

    const persona = await this.findOne(id);

    if (!persona) {
      throw new NotFoundException("No se pudo recuperar la persona.");
    }

    return persona;
  }

  async getPersonas(): Promise<Persona[]> {

    const personas = await this.find();

    if (!personas) {
      throw new NotFoundException("No se pudo recuperar las personas.");
    }

    return personas;
  }

  async postPersona(crearPersonaDto: PersonaAgregarDto): Promise<Persona> {

    const { nombres, identificacion, genero, edad, esEstudiante } = crearPersonaDto;

    const nuevaPersona = new Persona();
    nuevaPersona.nombres = nombres;
    nuevaPersona.edad = edad;
    nuevaPersona.identificacion = identificacion ? identificacion : null;
    nuevaPersona.genero = genero;
    nuevaPersona.esEstudiante = esEstudiante;

    try {
      await nuevaPersona.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe una persona con esa identificacion.');
      } else {
        throw new InternalServerErrorException();
      }
    }
    return nuevaPersona;

  }

  async updatePersona(idPersona: number, actualizarPersonaDto: PersonaActualizarDto): Promise<Persona> {

    const { nombres, edad, genero, identificacion } = actualizarPersonaDto;
    const persona = await this.getPersonasByID(idPersona);
    persona.nombres = nombres;
    persona.edad = edad;
    persona.genero = genero;
    persona.identificacion = identificacion ? identificacion : null;

    try {
      await persona.save();
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Ya existe una persona con esa identificacion.');
      } else {
        throw new InternalServerErrorException();
      }
    }

    return persona;
  }

}
