import { IsString, MaxLength, IsNumber, IsBoolean, IsInt, IsAlpha, IsOptional } from 'class-validator';

export class PersonaAgregarDto {

  //@IsAlpha()
  @MaxLength(100, { message: 'El nombre completo de la persona no puede rebasar los 100 caracteres.' })
  nombres: string;

  @IsString()
  @IsOptional()
  @MaxLength(20, { message: 'La identificación de la persona no puede sobrepasar los 20 caracteres.' })
  identificacion: string;

  @IsAlpha()
  genero: string;

  @IsInt({ message: 'La edad debe ser un número entero.' })
  edad: number;

  @IsBoolean()
  esEstudiante: boolean;
}
