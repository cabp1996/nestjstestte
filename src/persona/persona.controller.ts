import { Controller, Get, Post, Body, Patch, Param, ParseIntPipe, Delete, UseGuards, ValidationPipe } from '@nestjs/common';
import { PersonaService } from './persona.service';
import { Persona } from './persona.entity';
import { PersonaAgregarDto } from './DTO/persona-agregar.dto';
import { PersonaActualizarDto } from './DTO/persona-actualizar.dto';
import { AuthGuard } from '@nestjs/passport';


@Controller('persona')
@UseGuards(AuthGuard('jwt'))
export class PersonaController {

  constructor(private readonly _servicioPersona: PersonaService) {

  }


  @Get()
  getPersonas(): Promise<Persona[]> {
    return this._servicioPersona.getPersonas();
  }

  @Get('/:id')
  getPersonaByID(@Param('id', ParseIntPipe) id: number): Promise<Persona> {
    return this._servicioPersona.getPersonasByID(id);
  }

  @Post()
  postPersona(@Body(ValidationPipe) personaCrearDto: PersonaAgregarDto): Promise<Persona> {
    return this._servicioPersona.postPersona(personaCrearDto);
  }



  @Patch('/:id')
  updatePersona(@Param('id') id: number, @Body(ValidationPipe) personaActualizarDto: PersonaActualizarDto) {
    return this._servicioPersona.updatePersona(id, personaActualizarDto);
  }

  @Delete('/:id')
  deletePersona(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this._servicioPersona.deletePersona(id);
  }
}
