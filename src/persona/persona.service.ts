import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PersonaRepository } from './persona.repository';
import { Persona } from './persona.entity';
import { PersonaAgregarDto } from './DTO/persona-agregar.dto';
import { PersonaActualizarDto } from './DTO/persona-actualizar.dto';
import { TallerService } from '../taller/taller.service';

@Injectable()
export class PersonaService {

  constructor(@InjectRepository(PersonaRepository) private readonly _personaRepository: PersonaRepository,
    private readonly _tallerService: TallerService) {

  }

  async getPersonasByID(id: number): Promise<Persona> {
    return this._personaRepository.getPersonasByID(id);
  }

  async getPersonas(): Promise<Persona[]> {
    return this._personaRepository.getPersonas();
  }

  async postPersona(crearPersonaDto: PersonaAgregarDto): Promise<Persona> {
    return this._personaRepository.postPersona(crearPersonaDto);
  }


  async updatePersona(idPersona: number, actualizarPersonaDto: PersonaActualizarDto): Promise<Persona> {
    return this._personaRepository.updatePersona(idPersona, actualizarPersonaDto);
  }

  async deletePersona(idPersona: number): Promise<void> {
  
    const response = await this._personaRepository.query(`select fkTaller from registro_taller,estudiante,persona where registro_taller.fkEstudiante=estudiante.id and persona.id=estudiante.fkPersona and persona.id=${idPersona}`);
    const resultado = await this._personaRepository.delete(idPersona);
   
    if (resultado.affected === 0) {
      throw new NotFoundException("No existe una persona con el id: " + idPersona);
    } else {

      if (response.length !== 0) {
        response.forEach(async (taller) => {
          await this._tallerService.updateTallerParticipantes(Number(taller.fkTaller));
        });
      }
    }

  }

}
