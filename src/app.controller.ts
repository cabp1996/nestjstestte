import { Controller, Get } from '@nestjs/common';


@Controller('welcome')
export class AppController {

    @Get()
    getWelcome() {
        return 'Testing roboticas api v1.';
    }

    @Get('confirm')
    getConfirm() {
        return 'Working.';
    }

    @Get('presentation')
    getPresentation() {
        return 'API Nest JS.';
    }

}
