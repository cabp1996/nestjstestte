import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HistoricoTallerRepository } from './historico-taller.repository';
import { HistoricoTallerCrearDto } from './DTO/historico-taller-crear.dto';
import { HistoricoTaller } from './historico-taller.entity';


@Injectable()
export class HistoricoTallerService {

  constructor(@InjectRepository(HistoricoTallerRepository)
  private _repositoryHistorico: HistoricoTallerRepository) {

  }

  async createHistoricoTaller(crearHistoricoTallerDto: HistoricoTallerCrearDto): Promise<HistoricoTaller> {
    return this._repositoryHistorico.createHistoricoTaller(crearHistoricoTallerDto);
  }

  /*async createHistoricoEmocionesTaller(crearHistoricoTallerDto: HistoricoTallerCrearDto): Promise<HistoricoTaller> {
    return this._repositoryHistorico.createHistoricoTaller(crearHistoricoTallerDto);
  }*/

   guardarHistoricosTaller(crearHistoricoTallerDto: HistoricoTallerCrearDto[]): void {
    return this._repositoryHistorico.guardarHistoricosTaller(crearHistoricoTallerDto);
  }

  async obtenerHistoricosTallerPorEstudiante(idRegistroEstudianteTaller: number): Promise<HistoricoTaller> {
    //const estudianteBuscado = await this._servicioEstudiante.getEstudianteByID(idEstudiante);
    return this._repositoryHistorico.obtenerHistoricosTallerPorEstudiante(idRegistroEstudianteTaller);
  }

  async eliminarHistoricosSegunTallerId(idTaller: number): Promise<boolean> {

    const resultado = await this._repositoryHistorico.query('delete from historico_taller WHERE historico_taller.fkRegistroTaller IN(SELECT registro_taller.id from registro_taller where registro_taller.fkTaller=' + idTaller + ');');

    if (resultado.affectedRows === 0) {
      throw new NotFoundException("No se pudo borrar.");
    } else {
      return true;
    }
  }
}
