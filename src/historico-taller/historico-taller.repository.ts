import { Repository, EntityRepository } from "typeorm";
import { HistoricoTaller } from "./historico-taller.entity";
import { HistoricoTallerCrearDto } from "./DTO/historico-taller-crear.dto";
import { NotFoundException, InternalServerErrorException } from '@nestjs/common';

@EntityRepository(HistoricoTaller)
export class HistoricoTallerRepository extends Repository<HistoricoTaller>{

  async createHistoricoTaller(crearHistoricoTallerDto: HistoricoTallerCrearDto): Promise<HistoricoTaller> {

    const { registro, etapa, registroTaller, observaciones, fuente, tiempo } = crearHistoricoTallerDto;

    const nuevoHistorico = new HistoricoTaller();
    nuevoHistorico.registroTaller = registroTaller;
    nuevoHistorico.registro = registro;
    nuevoHistorico.etapa = etapa;
    nuevoHistorico.observaciones = observaciones ? observaciones : '';
    nuevoHistorico.fuente = fuente;
    nuevoHistorico.tiempo = tiempo;
    nuevoHistorico.url = '';

    if (nuevoHistorico.fuente === 2) {

      let historicoExistente: HistoricoTaller = await this.findOne({
        where: { etapa: etapa, tiempo: tiempo, registroTaller: registroTaller }
      });

      if (historicoExistente) {
        historicoExistente.registro = registro;
        await historicoExistente.save();
        return historicoExistente;
      }
    }

    await nuevoHistorico.save();

    return nuevoHistorico;
  }

  guardarHistoricosTaller(crearHistoricoTallerDto: HistoricoTallerCrearDto[]): void {

    let listaHistoricos: HistoricoTaller[] = [];

    crearHistoricoTallerDto.forEach((historico) => {

      const { registro, etapa, registroTaller, observaciones, fuente, tiempo, url } = historico;

      const nuevoHistorico = new HistoricoTaller();
      nuevoHistorico.registroTaller = registroTaller;
      nuevoHistorico.registro = registro;
      nuevoHistorico.etapa = etapa;
      nuevoHistorico.observaciones = observaciones ? observaciones : '';
      nuevoHistorico.fuente = fuente;
      nuevoHistorico.tiempo = tiempo;
      nuevoHistorico.url = url;

      try {

        nuevoHistorico.save();
        listaHistoricos.push(nuevoHistorico);

      } catch (error) {

        throw new InternalServerErrorException(error);
      }

    });
  }


  async obtenerHistoricosTallerPorEstudiante(idRegistroEstudianteTaller: number): Promise<HistoricoTaller> {

    const historicos = await this.find({
      join: {
        alias: "historico",
        leftJoinAndSelect: {
          registroTaller: "historico.registroTaller",
          etapa: "historico.etapa",
        },
      }
    });


    if (!historicos) {
      throw new NotFoundException("No hay registros de historico taller para el estudiante");
    }

    if (historicos.length > 0) {
      return historicos.find((historico) => { return historico.id === idRegistroEstudianteTaller });
    }

    return null;
  }
}
