import { RegistroTaller } from "../../registro-taller/registro-taller.entity";
import { Etapa } from "../../etapa/etapa.entity";

export class HistoricoTallerCrearDto {
  registro: string;
  observaciones: string;
  registroTaller: RegistroTaller;
  etapa: Etapa;
  fuente: number;
  tiempo: string;
  url: string;
}
