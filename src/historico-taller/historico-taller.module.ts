import { Module } from '@nestjs/common';
import { HistoricoTallerService } from './historico-taller.service';
import { HistoricoTallerController } from './historico-taller.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistoricoTallerRepository } from './historico-taller.repository';
import { EstudianteService } from '../estudiante/estudiante.service';
import { EstudianteRepository } from '../estudiante/estudiante.repository';
import { RegistroTallerService } from '../registro-taller/registro-taller.service';
import { RegistroTallerRepository } from '../registro-taller/registro-taller.repository';
import { TallerModule } from '../taller/taller.module';

@Module({
  imports: [TypeOrmModule.forFeature([HistoricoTallerRepository, EstudianteRepository, RegistroTallerRepository]), TallerModule],
  providers: [HistoricoTallerService, EstudianteService, RegistroTallerService],
  controllers: [HistoricoTallerController]
})
export class HistoricoTallerModule { }
