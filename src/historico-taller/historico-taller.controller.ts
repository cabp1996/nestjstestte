import { Controller, Get, Param, ParseIntPipe, Post, Body, Query, UseGuards, Delete } from '@nestjs/common';
import { HistoricoTallerService } from './historico-taller.service';
import { HistoricoTaller } from './historico-taller.entity';
import { HistoricoTallerCrearDto } from './DTO/historico-taller-crear.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('historico-taller')
@UseGuards(AuthGuard('jwt'))
export class HistoricoTallerController {

  constructor(private readonly _servicioHistoricoTaller: HistoricoTallerService) {

  }

  @Get()
  getHistoricosTallerByEstudiante(
    @Query('idRegistroTaller') idRegistroTaller: number): Promise<HistoricoTaller> {
    return this._servicioHistoricoTaller.obtenerHistoricosTallerPorEstudiante(idRegistroTaller);
  }

  @Post()
  crearHistoricoTaller(@Body() crearHistoricoDto: HistoricoTallerCrearDto): Promise<HistoricoTaller> {
    return this._servicioHistoricoTaller.createHistoricoTaller(crearHistoricoDto);
  }

  @Delete('/:id/tallerid')
  eliminarHistoricosTaller(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
    return this._servicioHistoricoTaller.eliminarHistoricosSegunTallerId(id);
  }
}
