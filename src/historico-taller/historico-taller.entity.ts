import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { Etapa } from "../etapa/etapa.entity";
import { RegistroTaller } from "../registro-taller/registro-taller.entity";



@Entity()
export class HistoricoTaller extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  registro: string;

  @Column({ length: 1000 })
  url: string;

  @Column({ length: 1000 })
  observaciones: string;

  @Column({ length: 10 })
  tiempo: string;

  @Column({ default: 1 })
  fuente: number;

  @ManyToOne(type => RegistroTaller, registroTaller => registroTaller.historicos, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fkRegistroTaller' })
  registroTaller: RegistroTaller;

  @ManyToOne(type => Etapa, etapa => etapa.historicos)
  @JoinColumn({ name: 'fkEtapa' })
  etapa: Etapa;

}
